<?php

namespace Drupal\vipps_recurring_payments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\vipps_recurring_payments\Service\VippsService;
use Drupal\vipps_recurring_payments\UseCase\Charges;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Charge Controller.
 *
 * @package Drupal\vipps_recurring_payments\Controller
 */
class ChargeController extends ControllerBase {

  /**
   * Http request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  private $request;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsService
   */
  private $vippsService;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * ChargeController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\vipps_recurring_payments\Service\VippsService $vippsService
   *   Vipps service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory.
   */
  public function __construct(
    RequestStack $requestStack,
    VippsService $vippsService,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->request = $requestStack->getCurrentRequest();
    $this->vippsService = $vippsService;
    $this->logger = $loggerChannelFactory;
  }

  /**
   * Make call.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function make() {
    try {
      $chargesStorage = new Charges($this->request->getContent());

      return new JsonResponse($this->vippsService->makeCharges($chargesStorage)->toArray());
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

  /**
   * Cancel call.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function cancel() {
    try {
      $chargesStorage = new Charges($this->request->getContent());

      return new JsonResponse($this->vippsService->cancelCharges($chargesStorage)->toArray());
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

  /**
   * Refund call.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function refund() {
    try {
      $chargesStorage = new Charges($this->request->getContent());

      return new JsonResponse($this->vippsService->refundCharges($chargesStorage)->toArray());
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

  /**
   * Get call.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function get() {
    try {
      $chargesStorage = new Charges($this->request->getContent());

      return new JsonResponse($this->vippsService->getCharges($chargesStorage)->toArray());
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

  /**
   * Create.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Drupal container.
   *
   * @return \Drupal\vipps_recurring_payments\Controller\ChargeController|static
   *   Charge interval.
   */
  public static function create(ContainerInterface $container) {
    /** @var \Symfony\Component\HttpFoundation\RequestStack $requestStack */
    $requestStack = $container->get('request_stack');

    /** @var \Drupal\vipps_recurring_payments\Service\VippsService $vippsService */
    $vippsService = $container->get('vipps_recurring_payments:vipps_service');

    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('logger.factory');

    return new static($requestStack, $vippsService, $loggerFactory);
  }

}
