<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\vipps_recurring_payments\Service\VippsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Agreement Controller.
 *
 * @package Drupal\vipps_recurring_payments\Controller
 */
class AgreementController extends ControllerBase {

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsService
   */
  private $vippsService;

  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  private $request;

  /**
   * AgreementController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\vipps_recurring_payments\Service\VippsService $vippsService
   *   Vipps service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel.
   */
  public function __construct(
    RequestStack $requestStack,
    VippsService $vippsService,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->request = $requestStack->getCurrentRequest();
    $this->vippsService = $vippsService;
    $this->logger = $loggerChannelFactory;
  }

  /**
   * Debug merchant agreement.
   */
  public function merchantAgreement() {
    $this->logger->get('vipps')->debug(Json::encode([$this->request->request, $this->request->query]));
  }

  /**
   * Get agreement call.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function get() {
    try {
      $requestContent = Json::decode($this->request->getContent());

      return new JsonResponse($this->vippsService->getAgreement($requestContent));
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

  /**
   * Cancel agreement call.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function cancel() {
    try {
      $requestContent = Json::decode($this->request->getContent());

      return new JsonResponse($this->vippsService->cancelAgreement($requestContent)->toArray());
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

  /**
   * Create function.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Drupal Container.
   *
   * @return \Drupal\vipps_recurring_payments\Controller\AgreementController|static
   *   Agreement controller.
   */
  public static function create(ContainerInterface $container) {
    /** @var \Symfony\Component\HttpFoundation\RequestStack $requestStack */
    $requestStack = $container->get('request_stack');

    /** @var \Drupal\vipps_recurring_payments\Service\VippsService $vippsService */
    $vippsService = $container->get('vipps_recurring_payments:vipps_service');

    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('logger.factory');

    return new static($requestStack, $vippsService, $loggerFactory);
  }

  /**
   * Get user info from agreement.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   */
  public function userinfo(): JsonResponse {
    try {
      $requestContent = Json::decode($this->request->getContent());

      return new JsonResponse($this->vippsService->getUserInfo($requestContent));
    }
    catch (\Throwable $exception) {
      return new JsonResponse([
        'success' => FALSE,
        'error' => $exception->getMessage(),
      ]);
    }
  }

}
