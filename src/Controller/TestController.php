<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Test Controller.
 *
 * @package Drupal\vipps_recurring_payments\Controller
 */
class TestController extends ControllerBase {

  /**
   * Http Client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  private $httpClient;

  /**
   * TestController constructor.
   *
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Http client.
   */
  public function __construct(VippsHttpClient $httpClient) {
    $this->httpClient = $httpClient;
  }

  /**
   * Authenticate.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function auth() {
    return new Response($this->httpClient->auth());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient */
    $httpClient = $container->get('vipps_recurring_payments:http_client');

    return new static($httpClient);
  }

}
