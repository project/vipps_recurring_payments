<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\UseCase;

/**
 * Charge Item class.
 */
class ChargeItem {

  /**
   * Id.
   *
   * @var string
   */
  private $id;

  /**
   * Price.
   *
   * @var int
   */
  private $price;

  /**
   * Description.
   *
   * @var string|null
   */
  private $description;

  /**
   * Charge id.
   *
   * @var mixed|null
   */
  private $chargeId;

  /**
   * Construct.
   */
  public function __construct(string $id, int $price, string $description = NULL, $chargeId = NULL) {
    $this->id = $id;
    $this->price = $price;
    $this->description = $description;
    $this->chargeId = $chargeId;
  }

  /**
   * Return the Agreement id.
   */
  public function getAgreementId():string {
    return $this->id;
  }

  /**
   * Return the price.
   */
  public function getPrice():?float {
    return round($this->price / 100, 2);
  }

  /**
   * Return the description.
   */
  public function getDescription():string {
    return $this->description;
  }

  /**
   * Return the charge id.
   */
  public function getChargeId():?string {
    return $this->chargeId;
  }

  /**
   * Check has description.
   */
  public function hasDescription():bool {
    return !is_null($this->description);
  }

  /**
   * Check has charge id.
   */
  public function hasChargeId():bool {
    return !is_null($this->chargeId);
  }

}
