<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\UseCase;

use function GuzzleHttp\json_decode;

/**
 * Charge object.
 */
class Charges {

  /**
   * Charges array.
   *
   * @var array
   */
  private $charges = [];

  /**
   * Construct.
   */
  public function __construct(string $chargesJson) {
    try {
      $this->initChargesArr(json_decode($chargesJson));
    }
    catch (\Throwable $e) {
      \Drupal::service('logger.factory')->get('vipps')->error($e->getMessage());
    }
  }

  /**
   * Return Charges.
   *
   * @return ChargeItem[]
   *   The charge.
   */
  public function getCharges():array {
    return $this->charges;
  }

  /**
   * Charge array.
   */
  private function initChargesArr(array $decodedCharges) {
    foreach ($decodedCharges as $charge) {
      array_push(
        $this->charges, new ChargeItem(
          $charge->agreement_id,
          $charge->price ?? 0,
          $charge->description ?? '',
          $charge->charge_id ?? ''
        )
      );
    }
  }

}
