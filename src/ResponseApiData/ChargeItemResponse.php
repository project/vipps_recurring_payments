<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

use Psr\Http\Message\ResponseInterface;

/**
 * Class Charge Item Response.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class ChargeItemResponse {
  const STATUS_PENDING = 'PENDING';
  const STATUS_DUE = 'DUE';
  const STATUS_CHARGED = 'CHARGED';
  const STATUS_FAILED = 'FAILED';
  const STATUS_REFUNDED = 'REFUNDED';
  const STATUS_PARTIALLY_REFUNDED = 'PARTIALLY_REFUNDED';
  const STATUS_RESERVED = 'RESERVED';

  /**
   * Charge id.
   *
   * @var string
   */
  private $id;

  /**
   * Charge status.
   *
   * @var string
   */
  private $status;

  /**
   * Charge due date.
   *
   * @var \DateTime
   */
  private $due;

  /**
   * Charge amount.
   *
   * @var int
   */
  private $amount;

  /**
   * Charge description.
   *
   * @var string
   */
  private $description;

  /**
   * Charge type.
   *
   * @var string
   */
  private $type;

  /**
   * ChargeItemResponse constructor.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response.
   */
  public function __construct(ResponseInterface $response) {
    try {
      /** @var \stdClass $responseStd */
      $responseStd = json_decode($response->getBody()->getContents());

      $this->id = $responseStd->id;
      $this->status = $responseStd->status;
      $this->due = $responseStd->due;
      $this->amount = $responseStd->amount;
      $this->description = $responseStd->description;
      $this->type = $responseStd->type;
    }
    catch (\Throwable $exception) {

    }
  }

  /**
   * Check if has been charged.
   *
   * @return bool
   *   True or false.
   */
  public function charged():bool {
    return ($this->status === self::STATUS_CHARGED);
  }

  /**
   * Check can continue charging.
   *
   * @return bool
   *   True or false.
   */
  public function canContinue():bool {
    // @todo check with which statuses need to wait.
    return in_array($this->status,
      [
        self::STATUS_PENDING,
        self::STATUS_DUE,
      ]
    );
  }

  /**
   * Check if charge has failed.
   *
   * @return bool
   *   True or false.
   */
  public function failed():bool {
    // @todo check with which statuses need to close agreement.
    return in_array($this->status,
      [
        self::STATUS_FAILED,
        self::STATUS_REFUNDED,
        self::STATUS_PARTIALLY_REFUNDED,
        self::STATUS_RESERVED,
      ]
    );
  }

  /**
   * Get charge id.
   *
   * @return string
   *   Id.
   */
  public function getId():string {
    return $this->id;
  }

  /**
   * Get charge date.
   *
   * @return \DateTime
   *   Datetime.
   *
   * @throws \Exception
   */
  public function getDue():\DateTime {
    return new \DateTime($this->due);
  }

  /**
   * Get charge amount.
   *
   * @return int
   *   Amount.
   */
  public function getAmount():int {
    return intval($this->amount);
  }

  /**
   * Get charge description.
   *
   * @return string
   *   Description.
   */
  public function getDescription():string {
    return $this->description;
  }

  /**
   * Get charge type.
   *
   * @return string
   *   Type.
   */
  public function getType():string {
    return $this->type;
  }

  /**
   * Get charge status.
   *
   * @return string
   *   Status.
   */
  public function getStatus():string {
    return $this->status;
  }

}
