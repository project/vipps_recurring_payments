<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

/**
 * Class Create Charges Response.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class CreateChargesResponse {

  /**
   * Errors.
   *
   * @var array
   */
  private $errors = [];

  /**
   * Successes.
   *
   * @var array
   */
  private $successes = [];

  /**
   * Add success message to charge.
   *
   * @param string $id
   *   Charge id.
   */
  public function addSuccessCharge(string $id) {
    array_push($this->successes, $id);
  }

  /**
   * Add error.
   *
   * @param \Drupal\vipps_recurring_payments\ResponseApiData\ResponseErrorItem $errorItem
   *   Error.
   */
  public function addError(ResponseErrorItem $errorItem) {
    array_push($this->errors, $errorItem->toString());
  }

  /**
   * Charges response to array.
   *
   * @return array
   *   Charges response.
   */
  public function toArray():array {
    return [
      'successes' => $this->successes,
      'errors' => $this->errors,
    ];
  }

}
