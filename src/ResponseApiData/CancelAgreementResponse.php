<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

/**
 * Class Cancel Agreement Response.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class CancelAgreementResponse {

  /**
   * Errors.
   *
   * @var array
   */
  private $errors = [];

  /**
   * Sucess.
   *
   * @var array
   */
  private $successes = [];

  /**
   * Add successful id.
   *
   * @param string $id
   *   Id.
   */
  public function addSuccessCancel(string $id) {
    array_push($this->successes, $id);
  }

  /**
   * Add error information.
   *
   * @param \Drupal\vipps_recurring_payments\ResponseApiData\ResponseErrorItem $errorItem
   *   Error information.
   */
  public function addError(ResponseErrorItem $errorItem) {
    array_push($this->errors, $errorItem->toString());
  }

  /**
   * Transform cancel agreement response into array.
   *
   * @return array
   *   Cancel agreement response.
   */
  public function toArray():array {
    return [
      'successes' => $this->successes,
      'errors' => $this->errors,
    ];
  }

}
