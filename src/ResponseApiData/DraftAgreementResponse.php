<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

/**
 * Class Draft Agreement Response.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class DraftAgreementResponse {

  /**
   * Confirmation URL.
   *
   * @var string
   */
  private $vippsConfirmationUrl;

  /**
   * Agreement id.
   *
   * @var string
   */
  private $agreementId;

  /**
   * DraftAgreementResponse constructor.
   *
   * @param string $vippsConfirmationUrl
   *   Confirmation URL.
   * @param string $agreementId
   *   Agreement id.
   */
  public function __construct(string $vippsConfirmationUrl, string $agreementId) {
    $this->vippsConfirmationUrl = $vippsConfirmationUrl;
    $this->agreementId = $agreementId;
  }

  /**
   * Get the confirmation URL.
   *
   * @return string
   *   Confirmation URL.
   */
  public function getVippsConfirmationUrl():string {
    return $this->vippsConfirmationUrl;
  }

  /**
   * Get the agreement id.
   *
   * @return string
   *   Agreement id.
   */
  public function getAgreementId():string {
    return $this->agreementId;
  }

}
