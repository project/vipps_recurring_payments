<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

/**
 * Class Response Error Item.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class ResponseErrorItem {

  /**
   * Error id.
   *
   * @var string
   */
  private $id;

  /**
   * Error message.
   *
   * @var string
   */
  private $message;

  /**
   * ResponseErrorItem constructor.
   *
   * @param string $id
   *   Error id.
   * @param string $message
   *   Error message.
   */
  public function __construct(string $id, string $message) {
    $this->message = $message;
    $this->id = $id;
  }

  /**
   * Get response error id.
   *
   * @return string
   *   Id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Get response error message.
   *
   * @return string
   *   Message.
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * To string function.
   *
   * @return string
   *   To string.
   */
  public function toString():string {
    return sprintf("%s: %s", $this->getId(), $this->getMessage());
  }

}
