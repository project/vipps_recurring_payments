<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

/**
 * Class Agreement Data.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class UserInfoData {

  /**
   * Email.
   *
   * @var string
   */
  private string $email;

  /**
   * Email verified or not.
   *
   * If the e-mail address that is delivered has the flag email_verified: false.
   * This address should not be used to link the user to an existing account,
   * without further authentication.
   *
   * @var bool
   */
  private bool $emailVerified;

  /**
   * First name.
   *
   * @var string
   */
  private string $firstName;

  /**
   * Last name.
   *
   * @var string
   */
  private string $lastName;

  /**
   * Personnummer (nin).
   *
   * @var string
   */
  private string $nin;

  /**
   * Phone number.
   *
   * @var string
   */
  private string $phoneNumber;

  /**
   * Postal code (Zip code).
   *
   * @param string
   */
  private string $postalCode;

  /**
   * City.
   *
   * @param string
   */
  private string $city;

  /**
   * Street address.
   *
   * @param string
   */
  private string $streetAddress;

  /**
   * Accounts.
   *
   * @var array
   * {
   *  "account_name": "string",
   *  "account_number": "string",
   *  "bank_name": "string"
   * }
   */
  private array $accounts;


  /**
   * Response.
   *
   * @var object
   */
  private object $response;

  /**
   * AgreementData constructor.
   *
   * @param object $response
   *   Response.
   */
  public function __construct(object $response) {
    $this->response = $response;
    $this->email = $response->email;
    $this->emailVerified = $response->email_verified ?? FALSE;
    $this->firstName = $response->given_name;
    $this->lastName = $response->family_name;
    $this->nin = $response->nin;
    $this->phoneNumber = $response->phone_number;
    $this->postalCode = $response->address->postal_code;
    $this->city = $response->address->region;
    $this->streetAddress = $response->address->street_address;
    $this->accounts = $response->accounts ?? [];
  }

  /**
   * Get email address.
   *
   * @return string
   *   email.
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * Is email verified or not.
   *
   * @return bool
   *   Email verified.
   */
  public function isEmailVerified(): bool {
    return $this->emailVerified;
  }

  /**
   * Get first name.
   *
   * @return string
   *   First name.
   */
  public function getFirstName(): string {
    return $this->firstName;
  }

  /**
   * Get last name.
   *
   * @return string
   *   Last name.
   */
  public function getLastName(): string {
    return $this->lastName;
  }

  /**
   * Get personnummer.
   *
   * @return string
   *   Personnummer.
   */
  public function getNin(): string {
    return $this->nin;
  }

  /**
   * Get phone number.
   *
   * @return string
   *   Phone number.
   */
  public function getPhoneNumber(): string {
    return $this->phoneNumber;
  }

  /**
   * Get postal code (zip code).
   *
   * @return string
   *   Postal code.
   */
  public function getPostalCode(): string {
    return $this->postalCode;
  }

  /**
   * Get city.
   *
   * @return string
   *   City.
   */
  public function getCity(): string {
    return $this->city;
  }

  /**
   * Get street address.
   *
   * @return string
   *   Street address.
   */
  public function getStreetAddress(): string {
    return $this->streetAddress;
  }

  /**
   * Get users bank accounts.
   *
   * @return array
   *   Accounts.
   */
  public function getAccounts(): array {
    return $this->accounts;
  }

  /**
   * Transform response into array.
   *
   * @return array
   *   Response.
   */
  public function toArray(): array {
    return (array) $this->response;
  }

}
