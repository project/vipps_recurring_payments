<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\ResponseApiData;

/**
 * Class Agreement Data.
 *
 * @package Drupal\vipps_recurring_payments\ResponseApiData
 */
class AgreementData {
  const PENDING = 'PENDING';
  const ACTIVE = 'ACTIVE';
  const STOPPED = 'STOPPED';
  const EXPIRED = 'EXPIRED';

  /**
   * Agreement id.
   *
   * @var string
   */
  private $id;

  /**
   * Agreement status.
   *
   * @var string
   */
  private $status;

  /**
   * Agreement price.
   *
   * @var int
   */
  private $price;

  /**
   * Response.
   *
   * @var object
   */
  private $response;

  /**
   * AgreementData constructor.
   *
   * @param object $response
   *   Response.
   */
  public function __construct(object $response) {
    if (!$this->statusValid($response->status)) {
      throw new \DomainException('Unsupported status');
    }
    $this->id = $response->id;
    $this->response = $response;
    $this->status = $response->status;
    $this->price = $response->pricing->amount;
  }

  /**
   * Get all status.
   *
   * @return string[]
   *   All status.
   */
  public function getStatuses(): array {
    return [
      self::PENDING,
      self::ACTIVE,
      self::STOPPED,
      self::EXPIRED,
    ];
  }

  /**
   * Get status is valid.
   *
   * @param string $status
   *   Status.
   *
   * @return bool
   *   True or false.
   */
  public function statusValid(string $status): bool {
    return in_array($status, $this->getStatuses());
  }

  /**
   * Get agreement id.
   *
   * @return string
   *   Agreement id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Get agreement status.
   *
   * @return string
   *   Agreement status.
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * Get agreement price.
   *
   * @return int
   *   Price.
   */
  public function getPrice():int {
    return $this->price;
  }

  /**
   * Get Subject - Identifier for the end user.
   *
   * @return string
   *   Sub.
   */
  public function getSub(): string {
    return $this->response->sub ?? '';
  }

  /**
   * Return User Info URL.
   *
   * @return string
   *   User info API endpoint.
   */
  public function getUserInfoUrl(): string {
    return $this->response->userinfoUrl ?? '';
  }

  /**
   * Return agreement is active.
   *
   * @return bool
   *   True or false.
   */
  public function isActive(): bool {
    return ($this->status === self::ACTIVE);
  }

  /**
   * Transform response into array.
   *
   * @return array
   *   Response.
   */
  public function toArray(): array {
    return (array) $this->response;
  }

}
