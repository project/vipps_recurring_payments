<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\vipps_recurring_payments\Factory\RequestStorageFactory;
use Drupal\vipps_recurring_payments\Repository\ProductSubscriptionRepositoryInterface;
use Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface;
use Drupal\vipps_recurring_payments\ResponseApiData\CancelAgreementResponse;
use Drupal\vipps_recurring_payments\ResponseApiData\CreateChargesResponse;
use Drupal\vipps_recurring_payments\ResponseApiData\ResponseErrorItem;
use Drupal\vipps_recurring_payments\UseCase\ChargeItem;
use Drupal\vipps_recurring_payments\UseCase\Charges;

/**
 * Vipps Service.
 */
class VippsService {

  /**
   * Http Vipps Client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  private $httpClient;

  /**
   * Request storage factory.
   *
   * @var \Drupal\vipps_recurring_payments\Factory\RequestStorageFactory
   */
  private $requestStorageFactory;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * Product Subscription repository.
   *
   * @var \Drupal\vipps_recurring_payments\Repository\ProductSubscriptionRepositoryInterface
   */
  private $productSubscriptionRepository;

  /**
   * Construct.
   */
  public function __construct(
    VippsHttpClient $httpClient,
    RequestStorageFactory $requestStorageFactory,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    ProductSubscriptionRepositoryInterface $productSubscriptionRepository
  ) {
    $this->httpClient = $httpClient;
    $this->requestStorageFactory = $requestStorageFactory;
    $this->logger = $loggerChannelFactory;
    $this->productSubscriptionRepository = $productSubscriptionRepository;
  }

  /**
   * Make charges.
   */
  public function makeCharges(Charges $chargesStorage):CreateChargesResponse {
    $token = $this->httpClient->auth();

    $response = new CreateChargesResponse();

    foreach ($chargesStorage->getCharges() as $charge) {
      try {
        $response->addSuccessCharge($this->createChargeItem($charge, $token));
      }
      catch (\Throwable $e) {
        $response->addError(new ResponseErrorItem($charge->getAgreementId(), $e->getMessage()));
      }
    }

    return $response;
  }

  /**
   * Cancel charges.
   */
  public function cancelCharges(Charges $chargesStorage):CreateChargesResponse {
    $token = $this->httpClient->auth();

    $response = new CreateChargesResponse();

    foreach ($chargesStorage->getCharges() as $charge) {

      try {
        $cancelResponse = $this->httpClient->cancelCharge($token, $charge->getAgreementId(), $charge->getChargeId());
        if ($cancelResponse['status'] == 200) {
          $response->addSuccessCharge($charge->getChargeId());
        }
        else {
          $response->addError(new ResponseErrorItem($charge->getChargeId(), Json::encode($cancelResponse)));
        }
      }
      catch (\Throwable $e) {
        $response->addError(new ResponseErrorItem($charge->getChargeId(), $e->getMessage()));
      }

    }

    return $response;
  }

  /**
   * Refund charges.
   */
  public function refundCharges(Charges $chargesStorage):CreateChargesResponse {
    $token = $this->httpClient->auth();

    $response = new CreateChargesResponse();

    foreach ($chargesStorage->getCharges() as $charge) {
      try {
        $product = $this->productSubscriptionRepository->getProduct();
        $product->setPrice($charge->getPrice());
        $product->setDescription($charge->getDescription());
        $request = $this->requestStorageFactory->buildCreateChargeData(
          $product,
          new \DateTime()
        );

        $refundResponse = $this->httpClient->refundCharge($token, $charge->getAgreementId(), $charge->getChargeId(), $request);
        if ($refundResponse['status'] == 200) {
          $response->addSuccessCharge($charge->getChargeId());
        }
        else {
          $response->addError(new ResponseErrorItem($charge->getChargeId(), Json::encode($refundResponse)));
        }
      }
      catch (\Throwable $e) {
        $response->addError(new ResponseErrorItem($charge->getChargeId(), $e->getMessage()));
      }

    }

    return $response;
  }

  /**
   * Get agreement.
   */
  public function getAgreement(string $agreementId) {
    $token = $this->httpClient->auth();

    try {
      $response = $this->httpClient->getRetrieveAgreement($token, $agreementId)->toArray();
    }
    catch (\Throwable $e) {
      $response = new ResponseErrorItem($agreementId, $e->getMessage());
    }

    return $response;
  }

  /**
   * Get user info.
   *
   * @param string $agreementId
   *   Agreement ID.
   *
   * @return array
   *   User info.
   */
  public function getUserInfo(string $agreementId): array {
    $token = $this->httpClient->auth();

    try {
      $agreementInfo = $this->httpClient->getRetrieveAgreement($token, $agreementId);
      if (!empty($agreementInfo->getSub()) && !empty($agreementInfo->getUserInfoUrl())) {
        return $this->httpClient->userInfo(
          $token,
          $agreementInfo->getUserInfoUrl()
        )->toArray();
      }
    }
    catch (\Throwable $e) {
      return ['error' => $e->getMessage()];
    }
    return [];
  }

  /**
   * Get charges.
   */
  public function getCharges(Charges $chargesStorage):CreateChargesResponse {
    $token = $this->httpClient->auth();

    $response = new CreateChargesResponse();

    foreach ($chargesStorage->getCharges() as $charge) {
      try {
        $response->addSuccessCharge(Json::encode($this->httpClient->getCharges($token, $charge->getAgreementId())));
      }
      catch (\Throwable $e) {
        $response->addError(new ResponseErrorItem($charge->getAgreementId(), $e->getMessage()));
      }
    }

    return $response;
  }

  /**
   * Check if the agreement is active.
   */
  public function agreementActive(string $agreementId):bool {
    return $this->httpClient->getRetrieveAgreement($this->httpClient->auth(), $agreementId)->isActive();
  }

  /**
   * Get the agreement status.
   */
  public function agreementStatus(string $agreementId):string {
    return $this->httpClient->getRetrieveAgreement($this->httpClient->auth(), $agreementId)->getStatus();
  }

  /**
   * Create a charge line.
   */
  public function createChargeItem(ChargeItem $chargeItem, string $token):string {
    $product = $this->productSubscriptionRepository->getProduct();
    $product->setPrice($chargeItem->getPrice());

    if ($chargeItem->hasDescription()) {
      $product->setDescription($chargeItem->getDescription());
    }

    if ($chargeItem->hasChargeId()) {
      $product->setOrderId($chargeItem->getChargeId());
    }

    $request = $this->requestStorageFactory->buildCreateChargeData(
      $product,
      new \DateTime()
    );
    return $this->httpClient->createCharge($token, $chargeItem->getAgreementId(), $request);
  }

  /**
   * Cancel the Agreement.
   */
  public function cancelAgreement(array $agreementIds):CancelAgreementResponse {
    $token = $this->httpClient->auth();

    $response = new CancelAgreementResponse();

    $product = $this->productSubscriptionRepository->getProduct();
    $request = $this->requestStorageFactory->buildCreateCancelData($product);

    foreach ($agreementIds as $agreementId) {
      $update = $this->updateAgreement($agreementId, $token, $request);
      if ($update['success']) {
        $response->addSuccessCancel($agreementId);
      }
      else {
        $response->addError($update[$agreementId]);
      }
    }
    return $response;
  }

  /**
   * Update the agreement.
   */
  public function updateAgreement(string $agreementId, string $token, RequestStorageInterface $request) {
    try {
      $response = $this->httpClient->updateAgreement($token, $agreementId, $request);
      if ($response->agreementId) {
        return ['success' => TRUE, $agreementId => TRUE];
      }
    }
    catch (\Throwable $e) {
      return [
        'success' => FALSE,
        $agreementId => new ResponseErrorItem($agreementId, $e->getMessage()),
      ];
    }
  }

}
