<?php

namespace Drupal\vipps_recurring_payments\Service;

/**
 * Charge Intervals Class.
 *
 * @package Drupal\vipps_recurring_payments\Service
 */
class ChargeIntervals {

  /**
   * Get intervals array.
   *
   * @param string $interval
   *   The interval.
   *
   * @return array
   *   Interval options array.
   *
   * @throws \Exception
   */
  public function getIntervals(string $interval = 'monthly'): array {
    switch ($interval) {
      case 'yearly':
        $intervals = [
          'base_interval'         => 'MONTH',
          'base_interval_count'   => 12,
        ];
        break;

      case 'monthly':
        $intervals = [
          'base_interval'         => 'MONTH',
          'base_interval_count'   => 1,
        ];
        break;

      case 'weekly':
        $intervals = [
          'base_interval'         => 'WEEK',
          'base_interval_count'   => 1,
        ];
        break;

      case 'daily':
        $intervals = [
          'base_interval'         => 'DAY',
          'base_interval_count'   => 1,
        ];
        break;

      default:
        throw new \Exception('Unsupported interval');
    }

    return $intervals;
  }

}
