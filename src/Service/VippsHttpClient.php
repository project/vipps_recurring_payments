<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Service;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface;
use Drupal\vipps_recurring_payments\ResponseApiData\AgreementData;
use Drupal\vipps_recurring_payments\ResponseApiData\ChargeItemResponse;
use Drupal\vipps_recurring_payments\ResponseApiData\DraftAgreementResponse;
use Drupal\vipps_recurring_payments\ResponseApiData\UserInfoData;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class VippsHttpClient calls.
 *
 * @package Drupal\vipps_recurring_payments\Service
 */
class VippsHttpClient {

  /**
   * Fallback value for the module version when using dev branch.
   */
  const MODULE_VERSION = '3.0.x-dev';

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * API configurations.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsApiConfig
   */
  private $config;

  /**
   * The module version.
   *
   * @var string
   */
  protected $version = self::MODULE_VERSION;

  /**
   * Vipps Http Client calls constructor.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   Http client.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The update registry service.
   * @param \Drupal\vipps_recurring_payments\Service\VippsApiConfig $config
   *   Configurations.
   */
  public function __construct(ClientInterface $httpClient, ModuleExtensionList $module_extension_list, VippsApiConfig $config) {
    $this->httpClient = $httpClient;
    $this->config = $config;

    // Try to get the module version from the registry.
    $list = $module_extension_list->getAllInstalledInfo();
    if (!empty($list['vipps_recurring_payments']['version'])) {
      $this->version = $list['vipps_recurring_payments']['version'];
    }
  }

  /**
   * Returns the module version.
   *
   * @return string
   *   Module version.
   */
  public function getVersion(): string {
    return $this->version;
  }

  /**
   * Authentication call.
   *
   * @return string
   *   Access token.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function auth():string {
    $response = $this->httpClient->request('POST', $this->config->getAccessTokenRequestUrl(), [
      'headers' => [
        'client_id' => $this->config->getClientId(),
        'client_secret' => $this->config->getClientSecret(),
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
    ]);

    return $this->getResponseBody($response)->access_token;
  }

  /**
   * Draft agreement call.
   *
   * @param string $token
   *   Access token.
   * @param \Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface $requestStorage
   *   Request storage.
   *
   * @return \Drupal\vipps_recurring_payments\ResponseApiData\DraftAgreementResponse
   *   Draft agreement response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function draftAgreement(string $token, RequestStorageInterface $requestStorage):DraftAgreementResponse {
    $response = $this->httpClient->request('POST', $this->config->getDraftAgreementRequestUrl(), [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
        'Idempotency-Key' => "{$requestStorage->getIdempotencyKey()}-{$requestStorage->getData()['pricing']['amount']}",
      ] + $this->getDefaultHeaders(),
      'json' => $requestStorage->getData(),
    ]);

    $responseData = $this->getResponseBody($response);

    return new DraftAgreementResponse(
      $responseData->vippsConfirmationUrl,
      $responseData->agreementId
    );
  }

  /**
   * Create charge call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   * @param \Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface $requestStorage
   *   Request storage.
   *
   * @return string
   *   Agreement id.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createCharge(string $token, string $agreementId, RequestStorageInterface $requestStorage):string {
    $response = $this->httpClient->request('POST', $this->config->getCreateChargeUrl($agreementId), [
      'Accept' => 'application/json',
      'headers' => [
        'Content-Type' => 'application/json',
        'Idempotency-Key' => $agreementId . time(),
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
      'json' => $requestStorage->getData(),
    ]);

    $responseData = $this->getResponseBody($response);

    return $responseData->chargeId;
  }

  /**
   * Get agreement call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   *
   * @return \Drupal\vipps_recurring_payments\ResponseApiData\AgreementData
   *   Agreement data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getRetrieveAgreement(string $token, string $agreementId):AgreementData {
    $response = $this->httpClient->request('GET', $this->config->getRetrieveAgreementUrl($agreementId), [
      'headers' => [
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
    ]);

    $responseData = $this->getResponseBody($response);
    return new AgreementData($responseData);
  }

  /**
   * Get single charge call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   * @param string $chargeId
   *   Charge id.
   *
   * @return \Drupal\vipps_recurring_payments\ResponseApiData\ChargeItemResponse
   *   Charge item response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getCharge(string $token, string $agreementId, string $chargeId):ChargeItemResponse {
    $response = $this->httpClient->request('GET', $this->config->getChargeUrl($agreementId, $chargeId), [
      'headers' => [
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
    ]);

    return new ChargeItemResponse($response);
  }

  /**
   * Get charges call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   *
   * @return mixed
   *   Json decoded body contents.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getCharges(string $token, string $agreementId) {
    $response = $this->httpClient->request('GET', $this->config->getRetrieveChargesUrl($agreementId), [
      'headers' => [
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
    ]);

    return json_decode($response->getBody()->getContents());
  }

  /**
   * Update agreement cll.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   * @param \Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface $requestStorage
   *   Request storage.
   *
   * @return object
   *   Json decoded response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function updateAgreement(string $token, string $agreementId, RequestStorageInterface $requestStorage) {
    $response = $this->httpClient->request('PATCH', $this->config->getUpdateAgreementUrl($agreementId), [
      'Accept' => 'application/json',
      'headers' => [
        'Content-Type' => 'application/json',
        'Idempotency-Key' => $agreementId . time(),
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
      'json' => $requestStorage->getData(),
    ]);

    return $this->getResponseBody($response);
  }

  /**
   * Cancel charge call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   * @param string $chargeId
   *   Charge id.
   *
   * @return array
   *   With Status code and error if so.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function cancelCharge(string $token, string $agreementId, string $chargeId) {
    $response = $this->httpClient->request('DELETE', $this->config->getChargeUrl($agreementId, $chargeId), [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
    ]);

    $statusCode = $response->getStatusCode();
    if ($statusCode == 200) {
      return [
        'status' => $statusCode,
      ];
    }
    else {
      return [
        'status' => $statusCode,
        'error' => $this->getResponseBody($response),
      ];
    }

  }

  /**
   * Refund charges call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   * @param string $chargeId
   *   Charge id.
   * @param \Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface $requestStorage
   *   Request storage.
   *
   * @return array
   *   With Status code and error if so.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function refundCharge(string $token, string $agreementId, string $chargeId, RequestStorageInterface $requestStorage) {
    $response = $this->httpClient->request('POST', $this->config->getRefundUrl($agreementId, $chargeId), [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
        'Idempotency-Key' => "{$agreementId}_{$chargeId}_{$requestStorage->getData()['amount']}",
      ] + $this->getDefaultHeaders(),
      'json' => $requestStorage->getData(),
    ]);

    $statusCode = $response->getStatusCode();
    if ($statusCode == 200) {
      return [
        'status' => $statusCode,
      ];
    }
    else {
      return [
        'status' => $statusCode,
        'error' => $this->getResponseBody($response),
      ];
    }

  }

  /**
   * Capture charge call.
   *
   * @param string $token
   *   Access token.
   * @param string $agreementId
   *   Agreement id.
   * @param string $chargeId
   *   Charge id.
   * @param \Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface $requestStorage
   *   Request storage.
   *
   * @return array
   *   With Status code and error if so.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function captureCharge(string $token, string $agreementId, string $chargeId, RequestStorageInterface $requestStorage) {
    $response = $this->httpClient->request('POST', $this->config->getCaptureUrl($agreementId, $chargeId), [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
        'Idempotency-Key' => "{$agreementId}_{$chargeId}_{$requestStorage->getData()['amount']}",
      ] + $this->getDefaultHeaders(),
      'json' => $requestStorage->getData(),
    ]);

    $statusCode = $response->getStatusCode();
    if ($statusCode == 200) {
      return [
        'status' => $statusCode,
      ];
    }
    else {
      return [
        'status' => $statusCode,
        'error' => $this->getResponseBody($response),
      ];
    }
  }

  /**
   * Get user info call.
   *
   * @param string $token
   *   Access token.
   * @param string $userInfoUrl
   *   Agreement id.
   *
   * @return \Drupal\vipps_recurring_payments\ResponseApiData\UserInfoData
   *   Agreement data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getUserInfo(string $token, string $userInfoUrl): UserInfoData {
    $response = $this->httpClient->request('GET', $userInfoUrl, [
      'headers' => [
        'Authorization' => "Bearer {$token}",
        'Ocp-Apim-Subscription-Key' => $this->config->getSubscriptionKey(),
      ] + $this->getDefaultHeaders(),
    ]);

    $responseData = $this->getResponseBody($response);
    return new UserInfoData($responseData);
  }

  /**
   * Get response body decoded.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The response.
   *
   * @return object
   *   JSON decoded response.
   */
  private function getResponseBody(ResponseInterface $response):\stdClass {
    return json_decode($response->getBody()->getContents());
  }

  /**
   * Returns default headers for every Vipps API call.
   *
   * @return array
   *   Default headers.
   */
  protected function getDefaultHeaders(): array {
    return [
      'Merchant-Serial-Number' => $this->config->getMsn(),
      'Vipps-System-Name' => 'Drupal',
      'Vipps-System-Version' => \Drupal::VERSION,
      'Vipps-System-Plugin-Name' => 'vipps-recurring-payments',
      'Vipps-System-Plugin-Version' => $this->getVersion(),
    ];
  }

}
