<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Service;

use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;

/**
 * Delay manager.
 */
class DelayManager {

  /**
   * Get Count the seconds to tomorrow.
   */
  public function getCountSecondsToTomorrow(int $hour = NULL, int $minute = NULL):int {
    $tomorrow = $this->getTomorrow();

    // @todo check 0 value
    if (!is_null($hour) && !is_null($minute)) {
      $tomorrow->setTime($hour, $minute);
    }

    return ($tomorrow->getTimestamp() - $this->getNow()->getTimestamp());
  }

  /**
   * Get Count the seconds to next payment.
   */
  public function getCountSecondsToNextPayment(ProductSubscriptionInterface $product):int {
    return (86400 * $product->getIntervalInDays() * $product->getIntervalCount());
  }

  /**
   * Get day after tomorrow datetime.
   */
  public function getDayAfterTomorrow():\DateTime {
    return (new \DateTime())->modify("+2 days");
  }

  /**
   * Get now datetime.
   */
  private function getNow():\DateTime {
    return new \DateTime();
  }

  /**
   * Get tomorrow datetime.
   */
  private function getTomorrow():\DateTime {
    return (new \DateTime())->modify("+1 day");
  }

}
