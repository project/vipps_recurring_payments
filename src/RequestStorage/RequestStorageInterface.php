<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\RequestStorage;

/**
 * Interface Request Storage Interface.
 *
 * @package Drupal\vipps_recurring_payments\RequestStorage
 */
interface RequestStorageInterface {

  /**
   * Get data.
   *
   * @return array
   *   Array.
   */
  public function getData():array;

  /**
   * Get idempotency key.
   *
   * @return string
   *   Idempotency key.
   */
  public function getIdempotencyKey(): string;

}
