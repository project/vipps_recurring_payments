<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\RequestStorage;

/**
 * Trait Price Trait.
 *
 * @package Drupal\vipps_recurring_payments\RequestStorage
 */
trait PriceTrait {

  /**
   * Get price.
   *
   * @param float|null $price
   *   Price.
   *
   * @return int
   *   Int price.
   */
  protected function getIntegerPrice(?float $price):int {
    return ($price * 100);
  }

}
