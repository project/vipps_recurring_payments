<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\RequestStorage;

use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;

/**
 * Class Create Charge Data.
 *
 * @package Drupal\vipps_recurring_payments\RequestStorage
 */
class CreateChargeData implements RequestStorageInterface {

  use PriceTrait;

  /**
   * Product.
   *
   * @var \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   */
  private $product;

  /**
   * Due datetime.
   *
   * @var \DateTime
   */
  private $due;

  /**
   * Number of retry days.
   *
   * @var int
   */
  private $retryDays;

  /**
   * CreateChargeData constructor.
   *
   * @param \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface $product
   *   Product.
   * @param \DateTime $due
   *   Due time.
   * @param int $retryDays
   *   Retry days number.
   */
  public function __construct(ProductSubscriptionInterface $product, \DateTime $due, int $retryDays) {
    $this->product = $product;
    $this->due = $due;
    $this->retryDays = $retryDays;
  }

  /**
   * Get the charge data as array.
   *
   * @return array
   *   Data.
   */
  public function getData(): array {
    $data = [
      "amount" => $this->product->getIntegerPrice(),
      "currency" => $this->product->getCurrency(),
      "description" => $this->product->getDescription(),
      "due" => $this->due->format("Y-m-d"),
      "hasPriceChanged" => FALSE,
      "retryDays" => $this->retryDays,
    ];

    if ($this->product->getOrderId()) {
      $data["orderId"] = $this->product->getOrderId();
    }

    return $data;
  }

}
