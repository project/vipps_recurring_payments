<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\RequestStorage;

use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;

/**
 * Class Cancel Agreement.
 *
 * @package Drupal\vipps_recurring_payments\RequestStorage
 */
class CancelAgreement implements RequestStorageInterface {
  const STATUS = 'STOPPED';

  /**
   * Product.
   *
   * @var \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   *   Product.
   */
  private $product;

  /**
   * CancelAgreement constructor.
   *
   * @param \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface $product
   *   Product.
   */
  public function __construct(ProductSubscriptionInterface $product) {
    $this->product = $product;
  }

  /**
   * Get data as array.
   *
   * @return string[]
   *   Data.
   */
  public function getData(): array {
    return [
      'status' => self::STATUS,
    ];
  }

}
