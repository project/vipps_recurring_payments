<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\RequestStorage;

use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;

/**
 * Class Draft Agreement Data.
 *
 * @package Drupal\vipps_recurring_payments\RequestStorage
 */
class DraftAgreementData implements RequestStorageInterface {

  /**
   * Is app.
   *
   * @var bool
   */
  private $isApp;

  /**
   * Merchant redirect URL.
   *
   * @var string
   */
  private $merchantRedirectUrl;

  /**
   * Merchant agreement URL.
   *
   * @var string
   */
  private $merchantAgreementUrl;

  /**
   * Customer phone number.
   *
   * @var string
   */
  private $customerPhoneNumber;

  /**
   * Idempotency-Key, unique API field.
   *
   * @var string
   */
  private $idempotencyKey;

  /**
   * Product subscription.
   *
   * @var \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   */
  private $product;

  /**
   * Is initial charge.
   *
   * @var bool
   */
  private $initialCharge;

  /**
   * Scope of what customer data should be returned from Vipps.
   *
   * @var string
   */
  private $scope;

  /**
   * DraftAgreementData constructor.
   *
   * @param \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface $product
   *   Product.
   * @param string $merchantRedirectUrl
   *   Merchant redirect url.
   * @param string $merchantAgreementUrl
   *   Merchant agreement url.
   * @param string $customerPhoneNumber
   *   Customer phone number.
   * @param bool $isApp
   *   Is app.
   * @param string $idempotencyKey
   *   Idempotency key.
   * @param string $scope
   *   Scope of what customer data should be returned from Vipps.
   */
  public function __construct(
    ProductSubscriptionInterface $product,
    string $merchantRedirectUrl,
    string $merchantAgreementUrl,
    string $customerPhoneNumber,
    bool $isApp,
    string $idempotencyKey,
    string $scope = ''
  ) {

    $this->product = $product;
    $this->merchantRedirectUrl = $merchantRedirectUrl;
    $this->merchantAgreementUrl = $merchantAgreementUrl;
    $this->customerPhoneNumber = $customerPhoneNumber;
    $this->isApp = $isApp;
    $this->initialCharge = $this->product->getInitialCharge();
    $this->idempotencyKey = $idempotencyKey;
    $this->scope = $scope;
  }

  /**
   * Get product.
   *
   * @return \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   *   Product subscription.
   */
  public function getProduct(): ProductSubscriptionInterface {
    return $this->product;
  }

  /**
   * Get draft agreement data as array.
   *
   * @return array
   *   Data.
   */
  public function getData(): array {
    $data = [
      "interval" => [
        "unit"  => $this->product->getIntervalValue(),
        "count" => $this->product->getIntervalCount(),
      ],
      "isApp" => $this->isApp,
      "merchantRedirectUrl" => $this->merchantRedirectUrl,
      "merchantAgreementUrl" => $this->merchantAgreementUrl,
      "pricing" => $this->product->getPricing(),
      "productDescription" => $this->product->getDescription(),
      "productName" => $this->product->getTitle(),
    ];
    if (!empty($this->customerPhoneNumber)) {
      $data['customerPhoneNumber'] = $this->customerPhoneNumber;
    }

    if ($this->scope != '') {
      $data['scope'] = $this->scope;
    }
    else {
      $scope = $this->product->getScope();
      $scope = trim($scope);
      if (!empty($scope)) {
        $data['scope'] = $scope;
      }
    }

    if ($this->initialCharge) {
      $data = array_merge($data, [
        "initialCharge" => [
          "amount" => $this->product->getIntegerPrice(),
          "currency" => $this->product->getCurrency(),
          "description" => $this->product->getDescription(),
          "transactionType" => "DIRECT_CAPTURE",
        ],
      ]);
      if ($this->product->getOrderId()) {
        $data["initialCharge"]['orderId'] = $this->product->getOrderId();
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdempotencyKey(): string {
    return $this->idempotencyKey;
  }

}
