<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Entity;

/**
 * Interface Product Subscription.
 *
 * @package Drupal\vipps_recurring_payments\Entity
 */
interface ProductSubscriptionInterface {

  /**
   * Get id.
   *
   * @return int
   *   Id.
   */
  public function getId():int;

  /**
   * Get title.
   *
   * @return string
   *   Title.
   */
  public function getTitle():string;

  /**
   * Set title.
   *
   * @param string $title
   *   Title.
   */
  public function setTitle(string $title):void;

  /**
   * Set interval value.
   *
   * @param string $interval
   *   Interval.
   */
  public function setIntervalValue(string $interval): void;

  /**
   * Set interval count.
   *
   * @param int $intervalCount
   *   Interval count.
   */
  public function setIntervalCount(int $intervalCount): void;

  /**
   * Get interval value.
   *
   * @return string
   *   Interval value.
   */
  public function getIntervalValue():string;

  /**
   * Get count interval.
   *
   * @return int
   *   Count interval.
   */
  public function getIntervalCount():int;

  /**
   * Set price.
   *
   * @param float|null $price
   *   Price.
   */
  public function setPrice(?float $price):void;

  /**
   * Get price.
   *
   * @return float|null
   *   Price.
   */
  public function getPrice():?float;

  /**
   * Get pricing array according to V3 API.
   *
   * @return array
   *   Pricing array.
   */
  public function getPricing():array;

  /**
   * Get integer price.
   *
   * @return int
   *   Price.
   */
  public function getIntegerPrice():int;

  /**
   * Get price as string.
   *
   * @return string
   *   Price as string.
   */
  public function getPriceAsString():string;

  /**
   * Get currency.
   *
   * @return string
   *   Currency.
   */
  public function getCurrency():string;

  /**
   * Get description.
   *
   * @return string
   *   Description.
   */
  public function getDescription():string;

  /**
   * Get interval in days.
   *
   * @return int
   *   Interval in days.
   */
  public function getIntervalInDays():int;

  /**
   * Set description.
   *
   * @param string $description
   *   Description.
   */
  public function setDescription(string $description): void;

  /**
   * Set initial charge.
   *
   * @param bool $initialCharge
   *   True or false.
   */
  public function setInitialCharge(bool $initialCharge): void;

  /**
   * Get is initial charge.
   *
   * @return bool
   *   True or false.
   */
  public function getInitialCharge(): bool;

  /**
   * Set scope of returned user data.
   *
   * @param string $scope
   *   Order id.
   */
  public function setScope(string $scope): void;

  /**
   * Get scope.
   *
   * @return string
   *   Scope.
   */
  public function getScope(): string;

}
