<?php

namespace Drupal\vipps_recurring_payments\Entity;

/**
 * Trait Interval In Days Trait.
 *
 * @package Drupal\vipps_recurring_payments\Entity
 * @method getIntervalValue
 */
trait IntervalInDaysTrait {

  /**
   * Get interval in days.
   *
   * @throws \Exception
   */
  public function getIntervalInDays():int {
    $interval = $this->getIntervalValue();

    switch ($interval) {
      case "DAY":
        return 1;

      case "WEEK":
        return 7;

      case "MONTH":
        // @todo check count days.
        return 30;

      default:
        throw new \Exception("Interval isn't supported");
    }
  }

}
