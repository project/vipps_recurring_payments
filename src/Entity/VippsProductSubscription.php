<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Entity;

/**
 * Class Vipps Product Subscription.
 *
 * @package Drupal\vipps_recurring_payments\Entity
 */
class VippsProductSubscription implements ProductSubscriptionInterface {
  use IntervalInDaysTrait;

  /**
   * Price.
   *
   * @var float
   */
  private $price;

  /**
   * Interval.
   *
   * @var string
   */
  private $interval;

  /**
   * Base interval count.
   *
   * @var int
   */
  private $intervalCount;

  /**
   * Product title.
   *
   * @var string
   */
  private $title;

  /**
   * Product description.
   *
   * @var string
   */
  private $description;

  /**
   * Is initial charge.
   *
   * @var bool
   */
  private $initialCharge;

  /**
   * Order id.
   *
   * @var string
   */
  private $orderId;

  /**
   * Scope.
   *
   * This used to tell Vipps API which user data needs to be returned.
   *
   * @var string
   */
  private $scope;

  /**
   * VippsProductSubscription constructor.
   *
   * @param string $baseInterval
   *   Base interval.
   * @param int $baseIntervalCount
   *   Base interval count.
   * @param string $title
   *   Title.
   * @param string $description
   *   Description.
   * @param bool $initialCharge
   *   Is initial charge.
   * @param string $orderId
   *   Order id.
   * @param string $scope
   *   Scope.
   */
  public function __construct(
    string $baseInterval = 'MONTH',
    int $baseIntervalCount = 1,
    string $title = '',
    string $description = '',
    bool $initialCharge = TRUE,
    string $orderId = '',
    string $scope = ''
  ) {
    $this->setIntervalValue($baseInterval);
    $this->setIntervalCount($baseIntervalCount);
    $this->setDescription($description);
    $this->setTitle($title);
    $this->setInitialCharge($initialCharge);
    $this->setOrderId($orderId);
    $this->setScope($scope);
  }

  /**
   * Get product id.
   */
  public function getId(): int {
    throw new \DomainException('id not supported');
  }

  /**
   * Get product title.
   *
   * @return string
   *   Title.
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * Set product title.
   *
   * @param string $title
   *   Title.
   */
  public function setTitle(string $title):void {
    $this->title = $title;
  }

  /**
   * Set interval.
   *
   * @param string $interval
   *   Interval.
   */
  public function setIntervalValue(string $interval): void {
    if (!in_array($interval, ['MONTH', 'WEEK', 'DAY'])) {
      throw new \DomainException();
    }

    $this->interval = $interval;
  }

  /**
   * Get interval.
   *
   * @return string
   *   Interval.
   */
  public function getIntervalValue(): string {
    return $this->interval;
  }

  /**
   * Set interval count.
   *
   * @param int $intervalCount
   *   Interval count.
   */
  public function setIntervalCount(int $intervalCount): void {
    $this->intervalCount = $intervalCount;
  }

  /**
   * Get interval.
   *
   * @return int
   *   Count interval.
   */
  public function getIntervalCount(): int {
    return $this->intervalCount;
  }

  /**
   * Get product price.
   *
   * @return float|null
   *   Price.
   */
  public function getPrice(): ?float {
    return round($this->price / 100, 2);
  }

  /**
   * Get pricing array according to V3 API.
   *
   * @return array
   *   Pricing array.
   */
  public function getPricing(): array {
    return [
      'amount' => $this->getIntegerPrice(),
      'currency' => $this->getCurrency(),
    ];
  }

  /**
   * Get price as int.
   *
   * @return int
   *   Price
   */
  public function getIntegerPrice(): int {
    return intval(round($this->price, 0));
  }

  /**
   * Get price as string.
   *
   * @return string
   *   Price.
   */
  public function getPriceAsString(): string {
    return strval($this->getPrice());
  }

  /**
   * Get currency.
   *
   * @return string
   *   Currency.
   */
  public function getCurrency(): string {
    return 'NOK';
  }

  /**
   * Set product description.
   *
   * @param string $description
   *   Description.
   */
  public function setDescription(string $description): void {
    $this->description = $description;
  }

  /**
   * Get product description.
   *
   * @return string
   *   Description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Set the price.
   *
   * @param float|null $price
   *   Price.
   */
  public function setPrice(?float $price):void {
    $this->price = round($price, 2) * 100;
  }

  /**
   * Get interval in days.
   *
   * @return int
   *   Interval in days.
   *
   * @throws \Exception
   */
  public function getIntervalInDays():int {
    switch ($this->getIntervalValue()) {
      case "DAY":
        return 1;

      case "WEEK":
        return 7;

      case "MONTH":
        // @todo check count days.
        return 30;

      default:
        throw new \Exception("Interval isn't supported");
    }
  }

  /**
   * Set initial charge.
   *
   * @param bool $initialCharge
   *   Is initial charge.
   */
  public function setInitialCharge(bool $initialCharge): void {
    $this->initialCharge = $initialCharge;
  }

  /**
   * Get initial charge.
   *
   * @return bool
   *   Is initial charge.
   */
  public function getInitialCharge(): bool {
    return $this->initialCharge;
  }

  /**
   * Set the order id.
   *
   * @param string $orderId
   *   Order id.
   */
  public function setOrderId(string $orderId): void {
    $this->orderId = $orderId;
  }

  /**
   * Get order id.
   *
   * @return string
   *   Order id.
   */
  public function getOrderId(): string {
    return $this->orderId;
  }

  /**
   * Set scope of returned user data.
   *
   * @param string $scope
   *   Order id.
   */
  public function setScope(string $scope): void {
    $this->scope = trim($scope);
  }

  /**
   * Get scope.
   *
   * @return string
   *   Scope.
   */
  public function getScope(): string {
    return $this->scope;
  }

}
