<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments\Factory;

use Detection\MobileDetect;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;
use Drupal\vipps_recurring_payments\Form\SettingsForm;
use Drupal\vipps_recurring_payments\RequestStorage\CancelAgreement;
use Drupal\vipps_recurring_payments\RequestStorage\CreateChargeData;
use Drupal\vipps_recurring_payments\RequestStorage\DraftAgreementData;
use Drupal\vipps_recurring_payments\RequestStorage\RequestStorageInterface;
use Drupal\vipps_recurring_payments\Service\DelayManager;
use Drupal\vipps_recurring_payments\Service\VippsApiConfig;

/**
 * Request Storage Factory.
 */
class RequestStorageFactory {

  /**
   * Vipps Api Config.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsApiConfig
   */
  protected $vippsApiConfig;

  /**
   * Configurations.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Delay Manager.
   *
   * @var \Drupal\vipps_recurring_payments\Service\DelayManager
   */
  protected $delayManager;

  /**
   * Mobile detect.
   *
   * @var Mobile_Detect
   */
  private $mobileDetect;

  /**
   * Construct.
   */
  public function __construct(
    VippsApiConfig $vippsApiConfig,
    ConfigFactoryInterface $configFactory,
    DelayManager $delayManager,
    MobileDetect $mobile_detect
  ) {
    $this->vippsApiConfig = $vippsApiConfig;
    $this->config = $configFactory->getEditable(SettingsForm::SETTINGS);
    $this->delayManager = $delayManager;
    $this->mobileDetect = $mobile_detect;
  }

  /**
   * Build the default Agreement data.
   */
  public function buildDefaultDraftAgreement(
    ProductSubscriptionInterface $product,
    string $phone,
    array $redirectPageGetParams = [],
    string $idempotencyKey = '',
    string $scope = ''
  ):DraftAgreementData {
    return new DraftAgreementData(
      $product,
      $this->vippsApiConfig->getMerchantRedirectUrl($redirectPageGetParams),
      $this->vippsApiConfig->getMerchantAgreementUrl($redirectPageGetParams),
      $phone,
      $this->mobileDetect->isMobile(),
      $idempotencyKey,
      $scope
    );
  }

  /**
   * Build the Charge data.
   */
  public function buildCreateChargeData(ProductSubscriptionInterface $product, \DateTime $dateTime):RequestStorageInterface {
    return new CreateChargeData(
      $product,
      // Two days in the future.
      $this->delayManager->getDayAfterTomorrow(),
      intval($this->config->get('charge_retry_days'))
    );
  }

  /**
   * Build the Cancel data.
   */
  public function buildCreateCancelData(ProductSubscriptionInterface $product):RequestStorageInterface {
    return new CancelAgreement($product);
  }

}
