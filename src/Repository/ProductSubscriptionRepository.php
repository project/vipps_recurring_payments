<?php

namespace Drupal\vipps_recurring_payments\Repository;

use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;
use Drupal\vipps_recurring_payments\Entity\VippsProductSubscription;

/**
 * Class Product Subscription Repository.
 *
 * @package Drupal\vipps_recurring_payments\Repository
 */
class ProductSubscriptionRepository implements ProductSubscriptionRepositoryInterface {

  /**
   * Get product subscription.
   *
   * @return \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   *   Product subscription.
   */
  public function getProduct():ProductSubscriptionInterface {
    return new VippsProductSubscription();
  }

}
