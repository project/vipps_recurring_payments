<?php

namespace Drupal\vipps_recurring_payments\Repository;

use Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface;

/**
 * Interface Product Subscription Repository.
 *
 * @package Drupal\vipps_recurring_payments\Repository
 */
interface ProductSubscriptionRepositoryInterface {

  /**
   * Get product subscription.
   *
   * @return \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   *   Product subscription.
   */
  public function getProduct():ProductSubscriptionInterface;

}
