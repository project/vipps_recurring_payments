<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments_webform\Service;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\vipps_recurring_payments\Entity\PeriodicCharges;
use Drupal\vipps_recurring_payments\Entity\VippsAgreements;
use Drupal\vipps_recurring_payments\Entity\VippsProductSubscription;
use Drupal\vipps_recurring_payments\Service\ChargeIntervals;
use Drupal\vipps_recurring_payments\Service\DelayManager;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;
use Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Class Agreement Service.
 *
 * @package Drupal\vipps_recurring_payments_webform\Service
 */
class AgreementService {

  /**
   * Http client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  private $httpClient;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * Submission repository.
   *
   * @var \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository
   */
  private $submissionRepository;

  /**
   * Delay manager.
   *
   * @var \Drupal\vipps_recurring_payments\Service\DelayManager
   */
  private $delayManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Charge intervals.
   *
   * @var \Drupal\vipps_recurring_payments\Service\ChargeIntervals
   */
  protected $chargeIntervals;

  /**
   * AgreementService constructor.
   *
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Http client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory.
   * @param \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository $submissionRepository
   *   Submission repository.
   * @param \Drupal\vipps_recurring_payments\Service\DelayManager $delayManager
   *   Delay manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\vipps_recurring_payments\Service\ChargeIntervals $chargeIntervals
   *   Charge intervals.
   */
  public function __construct(
    VippsHttpClient $httpClient,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    WebformSubmissionRepository $submissionRepository,
    DelayManager $delayManager,
    ModuleHandlerInterface $module_handler,
    AccountProxyInterface $currentUser,
    ChargeIntervals $chargeIntervals
  ) {
    $this->httpClient = $httpClient;
    $this->logger = $loggerChannelFactory;
    $this->submissionRepository = $submissionRepository;
    $this->delayManager = $delayManager;
    $this->moduleHandler = $module_handler;
    $this->currentUser = $currentUser;
    $this->chargeIntervals = $chargeIntervals;
  }

  /**
   * Confirm agreement and add next charge to queue.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $submission
   *   Webform submission.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function confirmAgreementAndAddChargeToQueue(WebformSubmissionInterface $submission):void {
    $agreementData = $this->httpClient->getRetrieveAgreement(
      $this->httpClient->auth(),
      $submission->getElementData('agreement_id')
    );

    $this->submissionRepository->setStatus($submission, $agreementData->getStatus());

    // If scope argument was set while calling for drafting agreement,
    // get user data.
    if ($agreementData->getUserInfoUrl() !== NULL) {
      $userInfo = $this->httpClient->getUserInfo($this->httpClient->auth(), $agreementData->getUserInfoUrl());
      if ($userInfo->getPhoneNumber()) {
        $this->submissionRepository->setEmail($submission, $userInfo->getEmail());
        $this->submissionRepository->setPhoneNumber($submission, $userInfo->getPhoneNumber());
        $this->submissionRepository->setFirstName($submission, $userInfo->getFirstName());
        $this->submissionRepository->setLastName($submission, $userInfo->getLastName());
        $this->submissionRepository->setAddress($submission, $userInfo->getStreetAddress());
        $this->submissionRepository->setPostalCode($submission, $userInfo->getPostalCode());
        $this->submissionRepository->setCity($submission, $userInfo->getCity());
        $this->submissionRepository->setNin($submission, $userInfo->getNin());
      }
    }

    if (!$agreementData->isActive()) {
      $this->logger->get('vipps')->error(
        sprintf("Agreement %s has status %s",
          $submission->getElementData('agreement_id'),
          $agreementData->getStatus()
        )
      );
    }

    // Invoke hook_vipps_recurring_payment_done and pass $submission.
    // This is useful so third party modules can do what ever they want after
    // form submission and payment.
    $this->moduleHandler->invokeAll('vipps_recurring_payment_done', ['submission' => $submission]);

    $webform = $submission->getWebform();
    $handlers = $webform->getHandlers();
    $handlerConfig = $handlers->getConfiguration();
    $configurations = $handlerConfig['vipps_agreement_handler'];
    $date = strtotime("now");

    /*  Create a Node of vipps_agreement type */
    $agreementNode = new VippsAgreements([
      'type' => 'vipps_agreements',
    ], 'vipps_agreements');
    $agreementNode->set('status', 1);
    $agreementNode->setStatus($agreementData->getStatus());
    $agreementNode->setIntervals($configurations['settings']['charge_interval'] ?? 'MONTHLY');
    $agreementNode->setAgreementId($submission->getElementData('agreement_id'));
    $agreementNode->setMobile($submission->getElementData('phone'));
    $agreementNode->setPrice($agreementData->getPrice());
    $agreementNode->setCreatedTime($date);
    $agreementNode->setChangedTime($date);
    $agreementNode->setRevisionCreationTime($date);
    $agreementNode->setOwnerId($this->currentUser->id());
    $agreementNode->setRevisionUserId($this->currentUser->id());

    $agreementNode->save();
    $agreementNodeId = $agreementNode->id();

    /* Store first charge as periodic_charges entity */
    $charges = $this->httpClient->getCharges(
      $this->httpClient->auth(),
      $submission->getElementData('agreement_id')
    );

    if (isset($charges)) {
      $chargeNode = new PeriodicCharges([
        'type' => 'periodic_charges',
      ], 'periodic_charges');
      $chargeNode->set('status', 1);
      $chargeNode->setChargeId($charges[0]->id);
      $chargeNode->setPrice($charges[0]->amount);
      $chargeNode->setParentId($agreementNodeId);
      $chargeNode->setStatus($charges[0]->status);
      $chargeNode->setDescription($charges[0]->description);
      $chargeNode->setRevisionCreationTime($date);
      $chargeNode->setCreatedTime($date);
      $chargeNode->setChangedTime($date);
      $chargeNode->setRevisionUserId($this->currentUser->id());
      $chargeNode->save();
    }

    if (isset($handlerConfig) && isset($handlerConfig['vipps_agreement_handler'])) {
      $intervals = $this->chargeIntervals->getIntervals($configurations['settings']['charge_interval']);

      $product = new VippsProductSubscription(
        $intervals['base_interval'],
        intval($intervals['base_interval_count']),
        $configurations['settings']['agreement_title'],
        $configurations['settings']['agreement_description'],
        boolval($configurations['settings']['initial_charge']),
      );
      $product->setPrice($this->getSubmissionAmount($submission));

      $job = Job::create('create_charge_job', [
        'orderId' => $submission->getElementData('agreement_id'),
        'agreementNodeId' => $agreementNodeId,
      ]);
      // @todo use custom queue.
      $queue = Queue::load('default');
      $queue->enqueueJob($job, $this->delayManager->getCountSecondsToNextPayment($product));

      $this->logger->get('vipps')->info(
        sprintf("Subscription %s has been done successfully", $submission->getElementData('agreement_id'))
      );
    }

  }

  /**
   * Get webform submission amount.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webformSubmission
   *   Webform submission.
   *
   * @return float
   *   Submission amount.
   */
  private function getSubmissionAmount(WebformSubmissionInterface $webformSubmission):float {
    $amount = !empty($webformSubmission->getElementData('amount_select')) ?
      $webformSubmission->getElementData('amount_select') :
      $webformSubmission->getElementData('amount');

    return floatval($amount);
  }

}
