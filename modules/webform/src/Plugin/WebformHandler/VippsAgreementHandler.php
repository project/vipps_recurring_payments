<?php

namespace Drupal\vipps_recurring_payments_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\vipps_recurring_payments\Entity\VippsProductSubscription;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "vipps_agreement_handler",
 *   label = @Translation("Vipps agreement handler"),
 *   category = @Translation("Webform Handler"),
 *   description = @Translation("This handler submits data to Vipps and stores agreement ID"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class VippsAgreementHandler extends WebformHandlerBase {

  /**
   * Vipps http client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  protected $httpClient;

  /**
   * Request storage factory.
   *
   * @var \Drupal\vipps_recurring_payments\Factory\RequestStorageFactory
   */
  protected $requestStorageFactory;

  /**
   * Submission repository.
   *
   * @var \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository
   */
  protected $submissionRepository;

  /**
   * Charge Intervals.
   *
   * @var \Drupal\vipps_recurring_payments\Service\ChargeIntervals
   */
  protected $chargeIntervals;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('vipps_recurring_payments:http_client');
    $instance->requestStorageFactory = $container->get('vipps_recurring_payments:request_storage_factory');
    $instance->submissionRepository = $container->get('vipps_recurring_payments_webform:submission_repository');
    $instance->chargeIntervals = $container->get('vipps_recurring_payments:charge_intervals');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'charge_interval' => 'monthly',
      'initial_charge' => 1,
      'agreement_title' => '',
      'agreement_description' => '',
      'scope' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['vipps_handler'] = [
      '#type' => 'details',
      '#title' => $this->t('Recurring configurations'),
      '#open' => TRUE,
    ];

    // Charge interval.
    $form['vipps_handler']['charge_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Charge interval'),
      '#required' => TRUE,
      '#options' => [
        'monthly' => $this->t('Monthly'),
        'weekly' => $this->t('Weekly'),
        'daily' => $this->t('Daily'),
        'yearly' => $this->t('Yearly'),
      ],
      '#default_value' => $this->configuration['charge_interval'],
      '#description' => $this->t('How often to charge'),
    ];

    // Initial charge.
    $form['vipps_handler']['initial_charge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Initial charge'),
      '#description' => $this->t('Charge money immediately when a subscription is created'),
      '#default_value' => $this->configuration['initial_charge'],
      '#return_value' => TRUE,
    ];

    // Agreement title.
    $form['vipps_handler']['agreement_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agreement title'),
      '#description' => $this->t('Title of the agreement'),
      '#default_value' => $this->configuration['agreement_title'],
      '#return_value' => TRUE,
    ];

    // Agreement description.
    $form['vipps_handler']['agreement_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agreement description'),
      '#description' => $this->t('Some description of the agreement'),
      '#default_value' => $this->configuration['agreement_description'],
      '#return_value' => TRUE,
    ];

    // User data to be returned.
    $form['vipps_handler']['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope: User data to be returned.'),
      '#description' => $this->t("Space separated list of the user profile-data scope to require for the agreement. If not required, leave empty.<br /> Allowed values are <b>address, birthDate, email, name, phoneNumber, nin, accountNumbers</b><br /> If this parameter has any value, after signing in the Vipps app, end user will be redirected to the same form."),
      '#default_value' => $this->configuration['scope'],
      '#return_value' => TRUE,
    ];

    $form = parent::buildConfigurationForm($form, $form_state);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * Confirm form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   * @param \Drupal\webform\WebformSubmissionInterface $webFormSubmission
   *   Webform submission.
   */
  public function confirmForm(array &$form, FormStateInterface $formState, WebformSubmissionInterface $webFormSubmission) {
    try {
      $intervals = $this->chargeIntervals->getIntervals($this->configuration['charge_interval']);

      $product = new VippsProductSubscription(
        $intervals['base_interval'],
        intval($intervals['base_interval_count']),
        $this->configuration['agreement_title'],
        $this->configuration['agreement_description'],
        $this->configuration['initial_charge'],
        $this->configuration['scope']
      );
      $product->setPrice($this->getAmount($formState));

      $draftAgreementResponse = $this->httpClient->draftAgreement(
        $this->httpClient->auth(),
        $this->requestStorageFactory->buildDefaultDraftAgreement(
          $product,
          $formState->getValue('phone'),
          ['submission_id' => $webFormSubmission->id()],
          $webFormSubmission->id()
        )
      );

      $this->submissionRepository->setAgreementId($webFormSubmission, $draftAgreementResponse->getAgreementId());

      new TrustedRedirectResponse($draftAgreementResponse->getVippsConfirmationUrl(), 302);
      $redirect = new RedirectResponse($draftAgreementResponse->getVippsConfirmationUrl(), 302);
      $redirect->send();
      die();
    }
    catch (\Throwable $exception) {
      $this->getLogger('vipps')->error($exception->getMessage());
    }
  }

  /**
   * Get amount.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return float|null
   *   Amount.
   */
  protected function getAmount(FormStateInterface $formState):? float {
    $amount = !empty($formState->getValue('amount_select')) ? $formState->getValue('amount_select') :
      $formState->getValue('amount');

    return floatval($amount);
  }

}
