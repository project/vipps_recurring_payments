<?php

namespace Drupal\vipps_recurring_payments_webform\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\vipps_recurring_payments\Entity\PeriodicCharges;
use Drupal\vipps_recurring_payments\Entity\VippsAgreements;
use Drupal\vipps_recurring_payments\Repository\ProductSubscriptionRepositoryInterface;
use Drupal\vipps_recurring_payments\Service\DelayManager;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;
use Drupal\vipps_recurring_payments\Service\VippsService;
use Drupal\vipps_recurring_payments\UseCase\ChargeItem;
use Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create Charge.
 *
 * @AdvancedQueueJobType(
 *   id = "create_charge_job",
 *   label = @Translation("Confirm charge queue"),
 * )
 */
class CreateCharge extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Product subscription.
   *
   * @var \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   */
  private $product;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsService
   */
  private $vippsService;

  /**
   * Webform submission.
   *
   * @var \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository
   */
  private $submissionRepository;

  /**
   * Vipps client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  private $httpClient;

  /**
   * Delay manager.
   *
   * @var \Drupal\vipps_recurring_payments\Service\DelayManager
   */
  private $delayManager;

  /**
   * CreateCharge constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel.
   * @param \Drupal\vipps_recurring_payments\Repository\ProductSubscriptionRepositoryInterface $productSubscriptionRepository
   *   Product subscription.
   * @param \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository $submissionRepository
   *   Webform submission.
   * @param \Drupal\vipps_recurring_payments\Service\VippsService $vippsService
   *   Vipps service.
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Vipps http client.
   * @param \Drupal\vipps_recurring_payments\Service\DelayManager $delayManager
   *   Delay Manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $loggerChannelFactory, ProductSubscriptionRepositoryInterface $productSubscriptionRepository, WebformSubmissionRepository $submissionRepository, VippsService $vippsService, VippsHttpClient $httpClient, DelayManager $delayManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->product = $productSubscriptionRepository->getProduct();
    $this->logger = $loggerChannelFactory->get('vipps');
    $this->vippsService = $vippsService;
    $this->submissionRepository = $submissionRepository;
    $this->httpClient = $httpClient;
    $this->delayManager = $delayManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('vipps_recurring_payments:product_subscription_repository'),
      $container->get('vipps_recurring_payments_webform:submission_repository'),
      $container->get('vipps_recurring_payments:vipps_service'),
      $container->get('vipps_recurring_payments:http_client'),
      $container->get('vipps_recurring_payments:delay_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    try {
      $payload = $job->getPayload();

      $agreementId = $payload['orderId'];
      $agreementNodeId = $payload['agreementNodeId'];
      $agreementNode = VippsAgreements::load($agreementNodeId);
      $agreementNode->getPrice();

      $chargeId = $this->vippsService->createChargeItem(
        new ChargeItem($agreementId, $this->product->getIntegerPrice()),
        $this->httpClient->auth()
      );

      // Get charge.
      $charge = $this->httpClient->getCharge($this->httpClient->auth(), $agreementId, $chargeId);
      // Store charge in periodic_charges entity.
      if (isset($charge)) {
        $chargeNode = new PeriodicCharges([
          'type' => 'periodic_charges',
        ], 'periodic_charges');
        $chargeNode->set('status', 1);
        $chargeNode->setChargeId($chargeId);
        $chargeNode->setPrice($charge->getAmount());
        $chargeNode->setParentId($agreementId);
        $chargeNode->setStatus($charge->getStatus());
        $chargeNode->setDescription($charge->getDescription());
        $chargeNode->save();

        // Add new job to queue for the next charge.
        $job = Job::create('create_charge_job', [
          'orderId' => $agreementId,
          'agreementNodeId' => $agreementNodeId,
        ]);
        // @todo use custom queue.
        $queue = Queue::load('default');
        $queue->enqueueJob($job, $this->delayManager->getCountSecondsToNextPayment($this->product));

        $this->logger->info(
          sprintf("Charge for %s has been done successfully", $agreementId)
        );
      }

      return JobResult::success();
    }
    catch (\Throwable $e) {
      return JobResult::failure($e->getMessage());
    }
  }

}
