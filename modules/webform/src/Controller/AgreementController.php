<?php

namespace Drupal\vipps_recurring_payments_webform\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository;
use Drupal\vipps_recurring_payments_webform\Service\AgreementService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Agreement Controller - Webform.
 *
 * @package Drupal\vipps_recurring_payments_webform\Controller
 */
class AgreementController extends ControllerBase {

  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Agreement service.
   *
   * @var \Drupal\vipps_recurring_payments_webform\Service\AgreementService
   */
  protected $agreementService;

  /**
   * Submission repository.
   *
   * @var \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository
   */
  protected $submissionRepository;

  /**
   * AgreementController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\vipps_recurring_payments_webform\Repository\WebformSubmissionRepository $submissionRepository
   *   Submission repository.
   * @param \Drupal\vipps_recurring_payments_webform\Service\AgreementService $agreementService
   *   Agreement service.
   */
  public function __construct(RequestStack $requestStack, WebformSubmissionRepository $submissionRepository, AgreementService $agreementService) {
    $this->request = $requestStack->getCurrentRequest();
    $this->submissionRepository = $submissionRepository;
    $this->agreementService = $agreementService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('vipps_recurring_payments_webform:submission_repository'),
      $container->get('vipps_recurring_payments_webform:agreement_service')
    );
  }

  /**
   * Confirm response.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to success front.
   */
  public function confirm(): RedirectResponse {
    $submission = $this->submissionRepository->getById(intval($this->request->get('submission_id')));

    try {
      $this->agreementService->confirmAgreementAndAddChargeToQueue($submission);
      $this->messenger()->addMessage($this->t('Subscription has been done successfully: %id', ['%id' => $submission->id()]));
    }
    catch (\Throwable $e) {
      $this->messenger()->addError($e->getMessage());
    }

    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

}
