<?php

declare(strict_types=1);

namespace Drupal\vipps_recurring_payments_webform\Repository;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\webform\WebformSubmissionInterface;
use League\Container\Exception\NotFoundException;

/**
 * Class Webform Submission Repository.
 *
 * @package Drupal\vipps_recurring_payments_webform\Repository
 */
class WebformSubmissionRepository {

  /**
   * Storage interface.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * WebformSubmissionRepository constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->storage = $entityTypeManager->getStorage('webform_submission');
  }

  /**
   * Get submission by id.
   *
   * @param int $id
   *   Submission id.
   *
   * @return \Drupal\webform\WebformSubmissionInterface
   *   Webform submission.
   */
  public function getById(int $id): WebformSubmissionInterface {
    /** @var \Drupal\webform\WebformSubmissionInterface $submission */
    $submission = $this->storage->load($id);

    if (is_null($submission)) {
      throw new NotFoundException();
    }

    return $submission;
  }

  /**
   * Set status from agreement.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $status
   *   Agreement status.
   */
  public function setStatus(WebformSubmissionInterface $entity, string $status): void {
    $entity->setElementData('agreement_status', $status);
    $entity->resave();
  }

  /**
   * Set agreement id.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $agreementId
   *   Agreement id.
   */
  public function setAgreementId(WebformSubmissionInterface $entity, string $agreementId): void {
    $entity->setElementData('agreement_id', $agreementId);
    $entity->resave();
  }

  /**
   * Set user email.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $email
   *   User email.
   */
  public function setEmail(WebformSubmissionInterface $entity, string $email): void {
    $entity->setElementData('email', $email);
    $entity->resave();
  }

  /**
   * Set user first name.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $firstName
   *   User firstName.
   */
  public function setFirstName(WebformSubmissionInterface $entity, string $firstName): void {
    $entity->setElementData('first_name', $firstName);
    $entity->resave();
  }

  /**
   * Set user last name.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $lastName
   *   User lastName.
   */
  public function setLastName(WebformSubmissionInterface $entity, string $lastName): void {
    $entity->setElementData('last_name', $lastName);
    $entity->resave();
  }

  /**
   * Set user address.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $address
   *   User address.
   */
  public function setAddress(WebformSubmissionInterface $entity, string $address): void {
    $entity->setElementData('address', $address);
    $entity->resave();
  }

  /**
   * Set user postal code.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $postalCode
   *   User postal code.
   */
  public function setPostalCode(WebformSubmissionInterface $entity, string $postalCode): void {
    $entity->setElementData('postal_code', $postalCode);
    $entity->resave();
  }

  /**
   * Set user city.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $city
   *   User city.
   */
  public function setCity(WebformSubmissionInterface $entity, string $city): void {
    $entity->setElementData('city', $city);
    $entity->resave();
  }

  /**
   * Set phone number.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $phone
   *   User city.
   */
  public function setPhoneNumber(WebformSubmissionInterface $entity, string $phone): void {
    $entity->setElementData('phone', $phone);
    $entity->resave();
  }

  /**
   * Set fødselsnummer (nin).
   *
   * @param \Drupal\webform\WebformSubmissionInterface $entity
   *   Webform submission.
   * @param string $nin
   *   User city.
   */
  public function setNin(WebformSubmissionInterface $entity, string $nin): void {
    $entity->setElementData('foedselnr', $nin);
    $entity->resave();
  }

}
