<?php

namespace Drupal\vipps_recurring_payments_commerce;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\vipps_recurring_payments\Entity\PeriodicCharges;
use Drupal\vipps_recurring_payments\Entity\VippsAgreements;
use Drupal\vipps_recurring_payments\Entity\VippsProductSubscription;
use Drupal\vipps_recurring_payments\Service\ChargeIntervals;
use Drupal\vipps_recurring_payments\Service\DelayManager;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;

/**
 * Provides the default recurring order manager.
 */
class RecurringOrderManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The vipps http client.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $httpClient;

  /**
   * Charge intervals.
   *
   * @var \Drupal\vipps_recurring_payments\Service\ChargeIntervals
   */
  private $intervalService;

  /**
   * Delay manager.
   *
   * @var \Drupal\vipps_recurring_payments\Service\DelayManager
   */
  private $delayManager;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new RecurringOrderManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Vipps http client.
   * @param \Drupal\vipps_recurring_payments\Service\ChargeIntervals $intervalService
   *   Interval service.
   * @param \Drupal\vipps_recurring_payments\Service\DelayManager $delayManager
   *   Delay manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, VippsHttpClient $httpClient, ChargeIntervals $intervalService, DelayManager $delayManager, LoggerChannelFactoryInterface $logger, AccountProxyInterface $currentUser) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $httpClient;
    $this->intervalService = $intervalService;
    $this->delayManager = $delayManager;
    $this->logger = $logger;
    $this->currentUser = $currentUser;
  }

  /**
   * Creates a recurring order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Drupal commerce order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The created recurring order, unsaved.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function createOrder(OrderInterface $order) {
    $orderItems = [];
    foreach ($order->getItems() as $orderItem) {
      $order_item = OrderItem::create([
        'type' => $orderItem->get('type')->getValue()[0]['target_id'],
        'purchased_entity' => $orderItem->getPurchasedEntity()->id(),
        'quantity' => $orderItem->getQuantity(),
        'unit_price' => $orderItem->getUnitPrice(),
      ]);
      $order_item->save();
      array_push($orderItems, $order_item);
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $newOrder */
    $newOrder = $this->entityTypeManager->getStorage('commerce_order')->create([
      'type' => 'vipps_recurring',
      'store_id' => $order->getStoreId(),
      'uid' => $order->getCustomerId(),
      'billing_profile' => $order ? $order->getBillingProfile() : NULL,
      'payment_gateway' => $order ? $order->get('payment_gateway')->first()->entity->id() : NULL,
      'order_items' => $orderItems,
    ]);

    $newOrder->setData('vipps_current_transaction', $order->getData('vipps_current_transaction'));
    $order_state = $newOrder->getState();
    $order_state_transitions = $order_state->getTransitions();
    $order_state->applyTransition($order_state_transitions['place']);
    $newOrder->save();
    $newOrder->setOrderNumber($newOrder->id());
    $newOrder->save();
    $this->logger->get('vipps_recurring_payments_commerce')->info($this->t('Recurring order created: %oid', ['%oid' => $newOrder->id()]));

    return $newOrder;
  }

  /**
   * Create vipps_agreement Node type and store 1 charge as periodic_charges.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Drupal commerce order.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   * @throws \Exception
   */
  public function createAgreementNode(OrderInterface $order) {
    $agreementId = $order->getData('vipps_current_transaction');
    $payment_gateway = $this->entityTypeManager
      ->getStorage('commerce_payment_gateway')
      ->load($order->payment_gateway->target_id);
    $intervals = $this->intervalService->getIntervals($payment_gateway->get('configuration')['charge_interval']);

    $agreementData = $this->httpClient->getRetrieveAgreement(
      $this->httpClient->auth(),
      $agreementId
    );

    $date = strtotime("now");

    // Create a Node of vipps_agreement type.
    $agreementNode = new VippsAgreements([
      'type' => 'vipps_agreements',
    ], 'vipps_agreements');
    $agreementNode->set('status', 1);
    $agreementNode->setStatus($agreementData->getStatus());
    $agreementNode->setIntervals($intervals['base_interval']);
    $agreementNode->setAgreementId($agreementId);

    // @todo add phone number.
    // $agreementNode->setMobile('').
    $agreementNode->setPrice($agreementData->getPrice());
    $agreementNode->setCreatedTime($date);
    $agreementNode->setChangedTime($date);
    $agreementNode->setOwnerId($this->currentUser->id());
    $agreementNode->setRevisionUserId($this->currentUser->id());
    $agreementNode->setRevisionCreationTime($date);

    $agreementNode->save();
    $agreementNodeId = $agreementNode->id();

    // Store first charge as periodic_charges entity.
    $charges = $this->httpClient->getCharges(
      $this->httpClient->auth(),
      $agreementId
    );

    if (isset($charges)) {
      $chargeNode = new PeriodicCharges([
        'type' => 'periodic_charges',
      ], 'periodic_charges');
      $chargeNode->set('status', 1);
      $chargeNode->setChargeId($charges[0]->id);
      $chargeNode->setPrice($charges[0]->amount);
      $chargeNode->setParentId($agreementNodeId);
      $chargeNode->setStatus($charges[0]->status);
      $chargeNode->setDescription($charges[0]->description);
      $chargeNode->setRevisionUserId($this->currentUser->id());
      $chargeNode->setRevisionCreationTime($date);
      $chargeNode->save();
    }

    $this->addToQueue($order, $agreementNode);
  }

  /**
   * Create a new order subscription.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Drupal commerce order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   Drupal commerce order.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function renewOrder(OrderInterface $order) {
    return $this->createOrder($order);
  }

  /**
   * Add job to queue.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Drupal commerce order.
   * @param \Drupal\vipps_recurring_payments\Entity\VippsAgreements $agreementNode
   *   Agreement node.
   *
   * @throws \Exception
   */
  private function addToQueue(OrderInterface $order, VippsAgreements $agreementNode) {
    $agreementData = $this->httpClient->getRetrieveAgreement(
      $this->httpClient->auth(),
      $order->getData('vipps_current_transaction')
    );

    $payment_gateways = $this->entityTypeManager
      ->getStorage('commerce_payment_gateway')
      ->loadMultipleForOrder($order);
    $payment_gateway = reset($payment_gateways);

    $intervals = $this->intervalService->getIntervals($payment_gateway->get('configuration')['charge_interval']);

    // Add a default value to title.
    $title = 'Recurring Payment';
    foreach ($order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();
      $title = $purchased_entity->getTitle();
    }

    $product = new VippsProductSubscription(
      $intervals['base_interval'],
      intval($intervals['base_interval_count']),
      $title,
      $title,
      $payment_gateway->get('configuration')['initial_charge']
    );
    $product->setPrice($agreementData->getPrice());

    $job = Job::create('create_charge_job_commerce', [
      'orderId' => $order->id(),
      'agreementId' => $order->getData('vipps_current_transaction'),
      'agreementNodeId' => $agreementNode->id(),
    ]);

    $queue = Queue::load('vipps_recurring_payments');
    $queue->enqueueJob($job, $this->delayManager->getCountSecondsToNextPayment($product));

    $message_variables['%aid'] = $order->getData('vipps_current_transaction');
    $this->logger->get('vipps_recurring_commerce')->info(
      'Order %oid: Subscription %aid has been done successfully', $message_variables
    );
  }

}
