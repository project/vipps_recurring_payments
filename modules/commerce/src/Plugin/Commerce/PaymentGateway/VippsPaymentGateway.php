<?php

namespace Drupal\vipps_recurring_payments_commerce\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\SoftDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\vipps_recurring_payments\Form\SettingsForm;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;
use Drupal\vipps_recurring_payments\Service\VippsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Vipps payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "vipps_payment_gateway",
 *   label = "Vipps Payment Gateway",
 *   display_label = "Vipps Payment Gateway",
 *    forms = {
 *     "offsite-payment" = "Drupal\vipps_recurring_payments_commerce\PluginForm\OffsiteRedirect\VippsPaymentGatewayCheckout",
 *   },
 *   requires_billing_information = TRUE,
 * )
 */
class VippsPaymentGateway extends OffsitePaymentGatewayBase implements SupportsAuthorizationsInterface, SupportsRefundsInterface, SupportsNotificationsInterface {

  use StringTranslationTrait;

  /**
   * Vipps http client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  protected $httpClient;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsService
   */
  protected $vippsService;

  /**
   * BaseVippsPaymentGateway constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Vipps http client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\vipps_recurring_payments\Service\VippsService $vippsService
   *   Vipps service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, VippsHttpClient $httpClient, ConfigFactoryInterface $configFactory, VippsService $vippsService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->vippsService = $vippsService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('vipps_recurring_payments:http_client'),
      $container->get('config.factory'),
      $container->get('vipps_recurring_payments:vipps_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = \Drupal::config('vipps_recurring_payments.settings');

    return [
      'msn' => $config->get('msn'),
      'subscription_key' => $config->get('subscription_key'),
      'client_id' => $config->get('client_id'),
      'client_secret' => $config->get('client_secret'),
      'charge_retry_days' => $config->get('charge_retry_days'),
      'charge_interval' => $config->get('charge_interval'),
      'initial_charge' => $config->get('initial_charge'),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\vipps_recurring_payments\Form\SettingsForm $plugin */
    $configFactory = $this->configFactory->getEditable(SettingsForm::SETTINGS);
    $rowData = $configFactory->getRawData();

    $mode = 'test';

    if (isset($rowData['test_mode'])) {
      if ($rowData['test_mode'] == 1) {
        $mode = 'test';
      }
      else {
        $mode = 'live';
      }
    }

    $form['charge_interval'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Charge Frequency'),
      '#default_value' => $this->configuration['charge_interval'] ?? 'monthly',
      '#options' => [
        'monthly' => $this->t('Monthly'),
        'weekly' => $this->t('Weekly'),
        'daily' => $this->t('Daily'),
      ],
    ];

    $form['initial_charge'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Initial Charge'),
      '#default_value' => $this->configuration['initial_charge'] ?? 'true',
      '#options' => [
        'true' => $this->t('True'),
        'false' => $this->t('False'),
      ],
    ];

    $form['mode']['#default_value'] = $mode;
    $form['mode']['#attributes']['readonly'] = 'readonly';
    $form['mode']['#attributes']['disabled'] = TRUE;
    $form['mode']['#description'] = $this->t('This setting is set on the main configuration at /admin/config/vipps/vipps-recurring-payments');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['charge_interval'] = $values['charge_interval'];
      $this->configuration['initial_charge'] = $values['initial_charge'];
    }
  }

  /**
   * Gets the Vipps service.
   */
  protected function getRequestStorageFactory() {
    return \Drupal::service('vipps_recurring_payments:request_storage_factory');
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $agreementId = $order->getData('vipps_current_transaction');
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $matching_payments = $payment_storage->loadByProperties([
      'remote_id' => $agreementId,
      'order_id' => $order->id(),
    ]);
    if (count($matching_payments) !== 1) {
      throw new PaymentGatewayException('More than one matching payment found');
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($matching_payments);

    // Get charge.
    try {
      $charges = $this->httpClient->getCharges($this->httpClient->auth(), $agreementId);
    }
    catch (\Exception $exception) {
      \Drupal::logger('vipps_recurring_commerce')->error(
        'Order %oid: Problems getting charge.', [
          '%oid' => $order->id(),
        ]
      );
      throw new SoftDeclineException($exception->getMessage());
    }

    $charge = end($charges);

    $order->setData('vipps_current_charge', $charge->id);
    $order->save();

    $agreementData = $this->httpClient->getRetrieveAgreement(
      $this->httpClient->auth(),
      $agreementId
    );

    $message_variables = [
      '%aid' => $agreementId,
      '%as' => $agreementData->getStatus(),
      '%oid' => $order->id(),
    ];

    switch ($agreementData->getStatus()) {
      case 'PENDING':
        $payment->setState('authorization');
        break;

      case 'ACTIVE':
        $payment->setState('completed');
        break;

      case 'STOPPED':
      case 'EXPIRED':
        $payment->setState('failed');
        $payment->save();
        $order->set('state', 'draft');
        $order->save();
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Oooops, something went wrong.', $message_variables
        );
        throw new PaymentGatewayException("Oooops, something went wrong.");

      default:
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Oooops, something went wrong.', $message_variables
        );
        throw new PaymentGatewayException("Oooops, something went wrong.");
    }

    // Set payment Status.
    switch ($charge->status) {
      case 'PENDING':
      case 'RESERVED':
        $payment->setState('authorization');
        break;

      case 'DUE':
      case 'CHARGED':
      case 'PROCESSING':
        $payment->setState('completed');
        break;

      case 'FAILED':
        $payment->setState('failed');
        break;

      case 'CANCELLED':
        $payment->setState('canceled');
        break;

      default:
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Oooops, something went wrong.', $message_variables
        );
    }

    $payment->save();

    if (!$agreementData->isActive()) {
      \Drupal::logger('vipps_recurring_commerce')->error(
        'Order %oid: Agreement %aid has status %as', $message_variables
      );
    }
  }

  /**
   * Vipps treats onReturn and onCancel in the same way.
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
  }

  /**
   * Checks for status changes, and saves it.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Http request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Http response.
   */
  public function onNotify(Request $request) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway */
    $commerce_payment_gateway = $request->attributes->get('commerce_payment_gateway');

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $request->attributes->get('order');
    if (!$order instanceof OrderInterface) {
      return new Response('', Response::HTTP_FORBIDDEN);
    }

    $order = Order::load($order->id());

    if (!$order) {
      \Drupal::logger('vipps_recurring_commerce')->error('No order found');
      return new Response('', Response::HTTP_FORBIDDEN);
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    // Validate authorization header.
    if ($order->getData('vipps_auth_key') !== $request->headers->get('Authorization')) {
      return new Response('', Response::HTTP_FORBIDDEN);
    }

    $content = $request->getContent();

    $remote_id = $request->attributes->get('remote_id');
    $matching_payments = $payment_storage->loadByProperties(
      [
        'remote_id' => $remote_id,
        'payment_gateway' => $commerce_payment_gateway->id(),
      ]
    );

    if (count($matching_payments) !== 1) {
      // @todo Log exception.
      return new Response('', Response::HTTP_FORBIDDEN);
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $matching_payment */
    $matching_payment = reset($matching_payments);

    $content = json_decode($content, TRUE);
    switch ($content['transactionInfo']['status']) {
      case 'RESERVED':
        $matching_payment->setState('authorization');
        break;

      case 'SALE':
        $matching_payment->setState('completed');
        break;

      case 'STOPPED':
      case 'EXPIRED':
        $matching_payment->setState('failed');
        $matching_payment->setRemoteState(Xss::filter($content['transactionInfo']['status']));
        break;

      default:
        \Drupal::logger('vipps_recurring_commerce')->critical('Data: @data', ['@data' => $content]);
        return new Response('', Response::HTTP_I_AM_A_TEAPOT);
    }
    $matching_payment->save();

    return new Response('', Response::HTTP_OK);
  }

  /**
   * Vipps treats capturePayment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    // Assert things.
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    if ($amount->lessThan($payment->getAmount())) {
      \Drupal::logger('vipps_recurring_commerce')->error(
        'Order %oid: Refund operation failed.', [
          '%oid' => $payment->getOrder()->id(),
        ]
      );
      throw new SoftDeclineException("Cannot capture a different amount");
    }

    $order = $payment->getOrder();
    $agreementId = $order->getData('vipps_current_transaction');
    $requestStorageFactory = \Drupal::service('vipps_recurring_payments:request_storage_factory');

    // Prevent the first orders where the order didn't have the charge id.
    if (strpos($order->getData('vipps_current_charge'), 'chr') === FALSE) {
      // First orders.
      // Get charge.
      try {
        $charges = $this->httpClient->getCharges($this->httpClient->auth(), $agreementId);
      }
      catch (\Exception $exception) {
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Refund operation failed.', [
            '%oid' => $order->id(),
          ]
        );
        throw new SoftDeclineException($exception->getMessage());
      }

      foreach ($charges as $value) {
        // Check if they were made on the same day.
        if (date('Y-m-d', $payment->getCompletedTime()) == date('Y-m-d', strtotime($value->due))) {
          $charge = $value;
          break;
        }
      }

      if (!isset($charge)) {
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Unable to find charge.', [
            '%oid' => $order->id(),
          ]
        );
        throw new SoftDeclineException('Order %oid: Unable to find charge.', [
          '%oid' => $order->id(),
        ]);
      }

      $order->setData('vipps_current_charge', $charge->id);
      $order->save();
    }

    // Get charge.
    $charge = $this->httpClient->getCharge($this->httpClient->auth(), $agreementId, $order->getData('vipps_current_charge'));

    if ($charge->getStatus() == 'RESERVED') {
      $product_repo = \Drupal::service('vipps_recurring_payments:product_subscription_repository');
      $product = $product_repo->getProduct();

      $product->setDescription($this->t('Capture %amo for Agreement %aid',
        [
          '%amo' => $amount->getNumber(),
          '%aid' => $agreementId,
        ]
      ));
      $product->setPrice($amount->getNumber());

      $request = $requestStorageFactory->buildCreateChargeData(
        $product,
        new \DateTime()
      );

      try {
        $this->httpClient->captureCharge($this->httpClient->auth(), $agreementId, $charge->getId(), $request);
      }
      catch (\Exception $exception) {
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Capture operation failed.', [
            '%oid' => $order->id(),
          ]
        );
        throw new DeclineException($exception->getMessage());
      }

      $payment->setState('completed');
      $payment->setAmount($amount);
      $payment->save();

      // Update parent payment if one exists.
      if (isset($parent_payment)) {
        $parent_payment->setAmount($parent_payment->getAmount()->subtract($amount));
        if ($parent_payment->getAmount()->isZero()) {
          $parent_payment->setState('authorization_voided');
        }
        $parent_payment->save();
      }

      \Drupal::logger('vipps_recurring_commerce')->info(
        'Order %oid: Payment Captured.', [
          '%oid' => $order->id(),
        ]
      );
    }
  }

  /**
   * Vipps treats refundPayment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    // Validate.
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // Let's do some refunds.
    parent::assertRefundAmount($payment, $amount);

    $order = $payment->getOrder();
    $agreementId = $order->getData('vipps_current_transaction');
    $requestStorageFactory = \Drupal::service('vipps_recurring_payments:request_storage_factory');

    // Prevent the first orders where the order didn't have the charge id.
    if (strpos($order->getData('vipps_current_charge'), 'chr') === FALSE) {
      // First orders.
      // Get charge.
      try {
        $charges = $this->httpClient->getCharges($this->httpClient->auth(), $agreementId);
      }
      catch (\Exception $exception) {
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Refund operation failed.', [
            '%oid' => $order->id(),
          ]
        );
        throw new SoftDeclineException($exception->getMessage());
      }

      foreach ($charges as $value) {
        // Check if they were made on the same day.
        if (date('Y-m-d', $payment->getCompletedTime()) == date('Y-m-d', strtotime($value->due))) {
          $charge = $value;
          break;
        }
      }

      if (!isset($charge)) {
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Order %oid: Unable to find charge.', [
            '%oid' => $order->id(),
          ]
        );
        throw new SoftDeclineException('Order %oid: Unable to find charge.', [
          '%oid' => $order->id(),
        ]);
      }

      $order->setData('vipps_current_charge', $charge->id);
      $order->save();
    }

    $product_repo = \Drupal::service('vipps_recurring_payments:product_subscription_repository');
    $product = $product_repo->getProduct();

    $product->setDescription($this->t('Refund %amo for Agreement %aid',
        [
          '%amo' => $amount->getNumber(),
          '%aid' => $agreementId,
        ]
      ));
    $product->setPrice($amount->getNumber());

    $request = $requestStorageFactory->buildCreateChargeData(
      $product,
      new \DateTime()
    );

    try {
      $this->httpClient->refundCharge($this->httpClient->auth(), $agreementId, $order->getData('vipps_current_charge'), $request);
    }
    catch (\Exception $exception) {
      \Drupal::logger('vipps_recurring_commerce')->error(
        'Order %oid: Refund operation failed.', [
          '%oid' => $order->id(),
        ]
      );
      throw new SoftDeclineException($exception->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();

    \Drupal::logger('vipps_recurring_commerce')->error(
      'Order %oid: Payment refunded.', [
        '%oid' => $order->id(),
      ]
    );
  }

  /**
   * Vipps treats voidPayment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    $order = $payment->getOrder();
    $agreementId = $order->getData('vipps_current_transaction');
    $token = $this->httpClient->auth();
    $chargeId = $payment->getRemoteId();

    try {
      $this->httpClient->cancelCharge($token, $agreementId, $chargeId);
    }
    catch (\Exception $exception) {
      \Drupal::logger('vipps_recurring_commerce')->error(
        'Order %oid: Void operation failed.', [
          '%oid' => $order->id(),
        ]
      );
      throw new DeclineException($exception->getMessage());
    }

    $payment->setState('authorization_voided');
    $payment->save();
    \Drupal::logger('vipps_recurring_commerce')->info(
      'Order %oid: Payment voided.', [
        '%oid' => $order->id(),
      ]
    );
  }

}
