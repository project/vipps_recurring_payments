<?php

namespace Drupal\vipps_recurring_payments_commerce\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\vipps_recurring_payments\Entity\PeriodicCharges;
use Drupal\vipps_recurring_payments\Repository\ProductSubscriptionRepositoryInterface;
use Drupal\vipps_recurring_payments\Service\DelayManager;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;
use Drupal\vipps_recurring_payments\Service\VippsService;
use Drupal\vipps_recurring_payments\UseCase\ChargeItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create Charge.
 *
 * @AdvancedQueueJobType(
 *   id = "create_charge_job_commerce",
 *   label = @Translation("Create charge queue - Commerce"),
 * )
 */
class CreateCharge extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Product subscription.
   *
   * @var \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface
   */
  private $product;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsService
   */
  private $vippsService;

  /**
   * Vipps client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  private $httpClient;

  /**
   * Vipps delay manager.
   *
   * @var \Drupal\vipps_recurring_payments\Service\DelayManager
   */
  private $delayManager;

  /**
   * CreateCharge constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel.
   * @param \Drupal\vipps_recurring_payments\Repository\ProductSubscriptionRepositoryInterface $productSubscriptionRepository
   *   Product subscription.
   * @param \Drupal\vipps_recurring_payments\Service\VippsService $vippsService
   *   Vipps service.
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Vipps http client.
   * @param \Drupal\vipps_recurring_payments\Service\DelayManager $delayManager
   *   Delay Manager.
   */
  public function __construct(
    LoggerChannelFactoryInterface $loggerChannelFactory,
    ProductSubscriptionRepositoryInterface $productSubscriptionRepository,
    VippsService $vippsService,
    VippsHttpClient $httpClient,
    DelayManager $delayManager
  ) {
    $this->product = $productSubscriptionRepository->getProduct();
    $this->logger = $loggerChannelFactory->get('vipps');
    $this->vippsService = $vippsService;
    $this->httpClient = $httpClient;
    $this->delayManager = $delayManager;
  }

  /**
   * Create function.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Drupal container.
   * @param array $configuration
   *   Drupal configurations.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   *
   * @return \Drupal\vipps_recurring_payments_commerce\Plugin\AdvancedQueue\JobType\CreateCharge|static
   *   Charge.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('logger.factory');

    /** @var \Drupal\vipps_recurring_payments\Entity\ProductSubscriptionInterface $productSubscriptionRepository */
    $productSubscriptionRepository = $container->get('vipps_recurring_payments:product_subscription_repository');

    /** @var \Drupal\vipps_recurring_payments\Service\VippsService $vippsService */
    $vippsService = $container->get('vipps_recurring_payments:vipps_service');

    /** @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient */
    $httpClient = $container->get('vipps_recurring_payments:http_client');

    /** @var \Drupal\vipps_recurring_payments\Service\DelayManager $delayManager */
    $delayManager = $container->get('vipps_recurring_payments:delay_manager');

    return new static(
      $loggerFactory,
      $productSubscriptionRepository,
      $vippsService,
      $httpClient,
      $delayManager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    try {
      $payload = $job->getPayload();

      $agreementId = $payload['agreementId'];
      $agreementNodeId = $payload['agreementNodeId'];

      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = Order::load($payload['orderId']);

      $message_variables = ['%aid' => $agreementId];

      try {
        $agreementIsActive = $this->vippsService->agreementActive($agreementId);
      }
      catch (\Exception $exception) {
        $message_variables['%m'] = $exception->getMessage();
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Agreement %aid: Problem getting the agreement status. Message: %m',
          $message_variables
        );
        throw new \Exception($exception->getMessage());
      }

      if (!$agreementIsActive) {
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Agreement %aid: The agreement it not ACTIVE',
          $message_variables
        );
        return JobResult::failure('The agreement it not ACTIVE');
      }

      foreach ($order->getItems() as $order_item) {
        $product_variation = $order_item->getPurchasedEntity();
        $title = $product_variation->getTitle();
      }

      /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
      $payment = Payment::create([
        'payment_gateway' => $order->get('payment_gateway')->first()->entity->id(),
        'order_id' => $order->id(),
        'amount' => $order->getTotalPrice(),
        'state' => 'new',
      ]);
      $payment->setRemoteId($order->getData('vipps_current_transaction'));
      $payment->save();

      \Drupal::logger('vipps_recurring_commerce')->info(
        'Order %oid: Payment %pid.', [
          '%oid' => $order->id(),
          '%pid' => $payment->id(),
        ]
      );

      try {
        $chargeId = $this->vippsService->createChargeItem(
          new ChargeItem($agreementId, $order->getTotalPrice()->getNumber() * 100, $title),
          $this->httpClient->auth()
        );
      }
      catch (\Exception $exception) {
        $message_variables['%m'] = $exception->getMessage();
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Agreement %aid: Problem creating charge. Message: %m',
          $message_variables
        );
        throw new Exception($exception->getMessage());
      }

      $payment->setState('completed');
      $payment->save();

      $order_state = $order->getState();
      $order_state_transitions = $order_state->getTransitions();
      // When it's "Draft", change to needs payment.
      if ($order_state == 'draft') {
        $order_state->applyTransition($order_state_transitions['place']);
      }
      $order_state->applyTransition($order_state_transitions['mark_paid']);
      $order->save();

      $message_variables['%cid'] = $chargeId;

      try {
        // Get charge.
        $charge = $this->httpClient->getCharge($this->httpClient->auth(), $agreementId, $chargeId);
      }
      catch (\Exception $exception) {
        $message_variables['%m'] = $exception->getMessage();
        \Drupal::logger('vipps_recurring_commerce')->error(
          'Agreement %aid: Problem getting charge %cid. Message: %m',
          $message_variables
        );
        throw new \Exception($exception->getMessage());
      }

      // Store charge in periodic_charges entity.
      if (isset($charge)) {
        $chargeNode = new PeriodicCharges([
          'type' => 'periodic_charges',
        ], 'periodic_charges');
        $chargeNode->set('status', 1);
        $chargeNode->setChargeId($chargeId);
        $chargeNode->setPrice($charge->getAmount());
        $chargeNode->setParentId($agreementId);
        $chargeNode->setStatus($charge->getStatus());
        $chargeNode->setDescription($charge->getDescription());
        $chargeNode->save();

        /** @var \Drupal\vipps_recurring_payments_commerce\RecurringOrderManager $recurringOrdermanager */
        $recurringOrderManager = \Drupal::service('vipps_recurring_payments_recurring.order_manager');

        $nex_order = $recurringOrderManager->renewOrder($order);

        // Add new job to queue for the next charge.
        $job = Job::create('create_charge_job_commerce', [
          'orderId' => $nex_order->id(),
          'agreementId' => $agreementId,
          'agreementNodeId' => $agreementNodeId,
        ]);

        $queue = Queue::load('vipps_recurring_payments');
        $queue->enqueueJob($job, $this->delayManager->getCountSecondsToNextPayment($this->product));

        $this->logger->info(
          sprintf("Charge for %s has been done successfully", $agreementId)
        );
      }

      return JobResult::success();
    }
    catch (\Throwable $e) {
      return JobResult::failure($e->getMessage());
    }
  }

}
