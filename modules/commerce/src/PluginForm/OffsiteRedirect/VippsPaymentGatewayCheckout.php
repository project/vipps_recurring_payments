<?php

namespace Drupal\vipps_recurring_payments_commerce\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\vipps_recurring_payments\Entity\VippsProductSubscription;
use Drupal\vipps_recurring_payments\Factory\RequestStorageFactory;
use Drupal\vipps_recurring_payments\Form\SettingsForm;
use Drupal\vipps_recurring_payments\Service\ChargeIntervals;
use Drupal\vipps_recurring_payments\Service\VippsHttpClient;
use Drupal\vipps_recurring_payments\Service\VippsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Vipps Payment Gateway Checkout.
 *
 * @package Drupal\vipps_recurring_payments_commerce\PluginForm\OffsiteRedirect
 */
class VippsPaymentGatewayCheckout extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * Vipps http client.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsHttpClient
   */
  protected $httpClient;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\VippsService
   */
  protected $vippsService;

  /**
   * Vipps service.
   *
   * @var \Drupal\vipps_recurring_payments\Service\ChargeIntervals
   */
  protected $chargeIntervals;

  /**
   * Request Storage Factory.
   *
   * @var \Drupal\vipps_recurring_payments\Factory\RequestStorageFactory
   */
  protected $requestStorageFactory;

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * BaseVippsPaymentGatewayForm constructor.
   *
   * @param \Drupal\vipps_recurring_payments\Service\VippsHttpClient $httpClient
   *   Vipps Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\vipps_recurring_payments\Service\VippsService $vippsService
   *   Vipps service.
   * @param \Drupal\vipps_recurring_payments\Service\ChargeIntervals $chargeIntervals
   *   Charge intervals.
   * @param \Drupal\vipps_recurring_payments\Factory\RequestStorageFactory $requestStorageFactory
   *   Request Storage Factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger factory.
   */
  public function __construct(VippsHttpClient $httpClient, ConfigFactoryInterface $configFactory, VippsService $vippsService, ChargeIntervals $chargeIntervals, RequestStorageFactory $requestStorageFactory, LoggerChannelFactoryInterface $logger) {
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->vippsService = $vippsService;
    $this->chargeIntervals = $chargeIntervals;
    $this->requestStorageFactory = $requestStorageFactory;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vipps_recurring_payments:http_client'),
      $container->get('config.factory'),
      $container->get('vipps_recurring_payments:vipps_service'),
      $container->get('vipps_recurring_payments:charge_intervals'),
      $container->get('vipps_recurring_payments:request_storage_factory'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var  \Drupal\commerce_order\Entity\Order $order */
    $order = $form_state->getFormObject()->getOrder();

    /** @var \Drupal\vipps_recurring_payments\Form\SettingsForm $settings */
    $configFactory = $this->configFactory->getEditable(SettingsForm::SETTINGS);
    $settings = $configFactory->getRawData();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $payment_gateway_settings = $payment_gateway_plugin->getConfiguration();

    if ($settings['msn'] == '' || $settings['test_msn'] == '') {
      $this->logger->get('vipps_recurring_commerce')->error(
        'There is no Settings for Vipps recurring'
      );
      throw new PaymentGatewayException('There is no Settings for Vipps recurring');
    }

    $token = $this->httpClient->auth();
    $order->setOrderNumber($order->id());
    $order->save();
    $title = ' ';

    // Can be considered an initial subscription order if it has at least one
    // product which has subscription enabled.
    foreach ($order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();
      $title = $purchased_entity->getTitle();
    }

    if ($order->getData('vipps_auth_key') === NULL) {
      $order->setData('vipps_auth_key', $token);
    }

    $intervals = $this->chargeIntervals->getIntervals($payment_gateway_settings['charge_interval']);

    $product = new VippsProductSubscription(
      $intervals['base_interval'],
      intval($intervals['base_interval_count']),
      $title,
      $title,
      $payment_gateway_settings['initial_charge']
    );
    $product->setPrice($order->getTotalPrice()->getNumber());

    try {
      $draftAgreementResponse = $this->httpClient->draftAgreement(
        $token,
        $this->requestStorageFactory->buildDefaultDraftAgreement(
          $product,
          '',
          [
            'commerce_order' => $order->id(),
            'step' => 'payment',
          ],
          $order->id()
        )
      );
    }
    catch (\Exception $exception) {
      $this->logger->get('vipps_recurring_commerce')->error(
        'Order %oid: problems drafting the agreement',
        ['%oid' => $order->id()]
      );
      throw new PaymentGatewayException($exception->getMessage());
    }

    $payment->setRemoteId($draftAgreementResponse->getAgreementId());
    $payment->save();

    $order->setData('vipps_current_transaction', $draftAgreementResponse->getAgreementId());
    $order->save();

    // Builds the redirect form.
    return $this->buildRedirectForm($form, $form_state, $draftAgreementResponse->getVippsConfirmationUrl(), []);
  }

}
