<?php

namespace Drupal\Tests\vipps_recurring_payments\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test that checking test mode does not crash.
 *
 * @group vipps_recurring_payments
 */
class TestModeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'vipps_recurring_payments',
    'views',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    // Import the default config from our module.
    $this->installConfig(['vipps_recurring_payments']);
    // But, in fact this is not enough to trigger the problem.
    $config_editable = $this->container->get('config.factory')
      ->getEditable('vipps_recurring_payments.settings');
    $config_editable
      ->set('msn', 'msn')
      ->set('subscription_key', 'subscription_key')
      ->set('client_id', 'client_id')
      ->set('client_secret', 'client_secret')
      ->set('MerchantAgreementUrl', 'http://example.com')
      ->set('test_mode', TRUE)
      ->set('test_msn', 'test_msn')
      ->set('test_subscription_key', 'test_subscription_key')
      ->set('test_client_id', 'test_client_id')
      ->set('test_client_secret', 'test_client_secret')
      ->save();
  }

  /**
   * Test a couple of variants of boolean that should all work.
   *
   * @dataProvider providerTestMode
   */
  public function testTestMode($value_to_set) {
    $this->container->get('config.factory')->getEditable('vipps_recurring_payments.settings')
      ->set('test_mode', $value_to_set)
      ->save();
    /** @var \Drupal\vipps_recurring_payments\Service\VippsApiConfig $service */
    $service = $this->container->get('vipps_recurring_payments:api_config');
    // This will trigger the getting of ::isTest.
    $url = $service->getBaseUrl();
    $is_considered_test = (bool) $value_to_set;
    self::assertEquals($is_considered_test ? 'https://apitest.vipps.no' : 'https://api.vipps.no', $url);
  }

  public static function providerTestMode() : array {
    return [
      [TRUE],
      [FALSE],
      ['TRUE'],
      ['FALSE'],
      ['true'],
      ['false'],
      ['1'],
      ['0'],
      [1],
      [0],
    ];
  }

}
